const fs = require("fs");
const pkg = require("../package.json");
const libPkg = require("../projects/smallstack/angular-components/package.json");

libPkg.version = pkg.version;

fs.writeFileSync("./projects/smallstack/angular-components/package.json", JSON.stringify(libPkg, null, 2));
console.log("wrote version " + libPkg.version + " to file: ./projects/smallstack/angular-components/package.json");
