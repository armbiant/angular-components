import { Directive, Host, Input, OnInit, Self } from "@angular/core";
import { MatSort } from "@angular/material/sort";
import { ApiQueryRequest } from "@smallstack/cloud-api-client";
import { PageableStore } from "@smallstack/store";


@Directive({
  selector: "[matSortStore]"
})
export class MatSortStoreDirective implements OnInit {

  @Input()
  public matSortStore: PageableStore;

  constructor(@Host() @Self() public matSort: MatSort) { }

  public ngOnInit() {
    if (this.matSortStore) {

      this.matSort.sortChange.subscribe(async (change: any) => {
        switch (change.direction) {
          case "asc":
            this.matSortStore.sortBy(change.active);
            break;
          case "desc":
            this.matSortStore.sortBy("-" + change.active);
            break;
          default:
            const query = this.matSortStore.query$.value;
            query.sort = undefined;
            this.matSortStore.query$.next(query);
            await this.matSortStore.load();
        }
      });

      this.evalApiRequest(this.matSortStore.query$.value);
    }
  }

  private evalApiRequest(apiQueryRequest: ApiQueryRequest) {
    if (apiQueryRequest?.sort) {
      let sort = apiQueryRequest.sort;
      if (sort) {
        const direction = sort.startsWith("-") ? "desc" : "asc";
        if (sort.startsWith("-"))
          sort = sort.substring(1);
        if (sort === "_id")
          sort = "id";
        this.matSort.direction = direction;
        this.matSort.active = sort;
      }
    }
  }

}
