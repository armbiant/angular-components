import { Directive, ElementRef } from "@angular/core";

@Directive({
  selector: "[sac-autofocus]"
})

export class SacAutofocusDirective {

  constructor(private elementRef: ElementRef) { }

  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }

}
