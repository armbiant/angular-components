import { Store } from "@smallstack/store";

export interface LocalStorageStoreOptions {
  /**
   * The local storage key
   */
  key: string;

  /**
   * The parser to use, defaults to "string"
   */
  parser?: "string" | "number" | "json";

  /**
   * if you want to set a default value, will be stored initially
   */
  default?: any;
}

/**
 * Autoloading store based on the browsers local storage which synchronizes automatically across browser tabs and sessions
 */
export class LocalStorageStore<T> extends Store<T> {
  constructor(private options: LocalStorageStoreOptions) {
    super(options.default);

    if (options.parser === undefined)
      options.parser = "string";

    // load initial value
    const initialValue: string = localStorage.getItem(options.key);
    if (initialValue !== null)
      this.setValue(this.parseValue(initialValue));
    else if (this.options.default)
      this.setValue(this.options.default);

    // react to changes from outside
    this.value$.subscribe((val) => {
      if (val === undefined)
        localStorage.removeItem(this.options.key);
      else
        localStorage.setItem(this.options.key, this.stringifyValue(val));
    });

    // react to changes from other tabs
    window.addEventListener("storage", (storageEvent: StorageEvent) => {
      if (storageEvent.key === this.options.key)
        if (storageEvent.newValue === null)
          this.setValue(this.options.default);
        else
          this.setValue(this.parseValue(storageEvent.newValue));
    });
  }

  private parseValue(val: any) {
    switch (this.options.parser) {
      case "json":
        return JSON.parse(val);
      case "number":
        return parseInt(val);
      case "string":
      default:
        return val;
    }
  }

  private stringifyValue(val: any): string {
    switch (this.options.parser) {
      case "json":
        return JSON.stringify(val);
      case "number":
        return "" + val;
      case "string":
      default:
        return val;
    }
  }
}
