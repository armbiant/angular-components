import { Pipe, PipeTransform } from "@angular/core";
import { TranslationService } from "../services/translation.service";



@Pipe({
  name: "ii18n"
})
export class II18nPipe implements PipeTransform {

  constructor(private translationService: TranslationService) {
  }

  public transform(value: string, ...args: any[]) {
    // tslint:disable-next-line: curly
    if (value !== undefined) {
      if (value.startsWith("@")) {
        const translated = this.translationService.getTranslation(value.substr(1).toLowerCase(), { locale: args[0] });
        if (translated)
          return translated;
        return "";
      }
      else return value;
    }
    return "";
  }

}
