import { Pipe, PipeTransform } from "@angular/core";
import { TranslationService } from "../services/translation.service";



@Pipe({
  name: "timezone"
})
export class TimezonePipe implements PipeTransform {

  public transform(value: number) {
    const str: string[] = ["GMT "];
    if (value >= 0)
      str.push("+");
    else
      str.push("-");
    if (value < 9 && value > -9)
      str.push("0");
    str.push("" + Math.abs(value));
    str.push(":00");
    // TODO: add sample cities
    return str.join("");
  }

}
