import { Params } from "@angular/router";

export interface WebComponentService {
  /**
   * Extracts the current URL query string of the Cloud Backoffice for the usage within a WebComponent, AngularComponents, etc.
   *
   * @returns {Params}
   * @memberof WebComponentService
   */
  getNavigationParams(): Params;

  /**
   * Method used for navigation from a WebComponent to another part of Cloud Backoffice, since the WebComponent itself does not have access to the Angular Router of the Cloud Backoffice.
   *
   * @param {string} targetPath path to navigate to in Cloud Backoffice. Origin is set to the Cloud Backoffice base URL
   * @param {(string | undefined)} [targetWebComponentTag] HTML-tag of another Custom Backoffice WebComponent. The mountpoint of the requested WebComponent is appended to the `targetPath`
   * @param {string} [queryParams] URL query parameters which are appended to navigation target
   * @returns {Promise<void>}
   * @memberof WebComponentService
   */
  navigateFromWebComponent(targetPath: string, targetWebComponentTag?: string | undefined, queryParams?: Params): Promise<void>;
}
