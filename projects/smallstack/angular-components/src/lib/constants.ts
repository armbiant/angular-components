import { InjectionToken } from "@angular/core";
import { PageableCrudStore } from "@smallstack/store";



export interface BackofficeEnvironment {
  production: boolean;
}


export const BACKOFFICE_ENVIRONMENT: InjectionToken<BackofficeEnvironment> = new InjectionToken<BackofficeEnvironment>("backoffice.environment");
export const PROJECT_TRANSLATION_STORE: InjectionToken<PageableCrudStore> = new InjectionToken<PageableCrudStore>("project.translation.store");

export const F_CHECKIN: string = "checkin";
export const T_CHECKIN_VISITOR: string = "visitor";
export const T_CHECKIN_VISIT_EVENT: string = "visitorevent";
export const T_CHECKIN_APP: string = "app";
export const T_CHECKIN_PRINTER: string = "printer";
export const T_CHECKIN_PRINTER_FORMAT: string = "printerformat";
export const T_CHECKIN_CONTACTPERSON: string = "contactperson";

export enum PAGINATOR_POSITION {
  TOP = "TOP",
  BOTTOM = "BOTTOM",
  BOTH = "BOTH"
}
