import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material/dialog";

@Component({
  templateUrl: "./refresh-dialog.component.html"
})
export class RefreshDialogComponent {
  constructor(public dialogRef: MatDialogRef<RefreshDialogComponent>) { }

  public onClose(refreshSite = false): void {
    this.dialogRef.close(refreshSite);
  }
}
