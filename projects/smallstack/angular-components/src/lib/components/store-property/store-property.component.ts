import { ChangeDetectorRef, Component, ContentChild, Input, OnChanges, OnInit, SimpleChanges, TemplateRef } from "@angular/core";
import { ObjectStore, PageableCrudStore, StoreState } from "@smallstack/store";

@Component({
  selector: "store-property",
  templateUrl: "store-property.component.html"
})
export class StorePropertyComponent implements OnChanges, OnInit {

  @Input()
  public store: ObjectStore | PageableCrudStore;

  @Input()
  public modelId: string;

  @Input()
  public property: string;

  @Input("notFound")
  public notFoundText: string;

  public notFound: boolean = false;

  @ContentChild(TemplateRef)
  public notFoundTemplate: TemplateRef<any>;

  public value: string;

  constructor(private cdr: ChangeDetectorRef) { }

  public ngOnInit(): void {
    // tslint:disable-next-line: no-string-literal
    if (this.store.state === StoreState.INITIAL && typeof this.store["reload"] === "function")
      // tslint:disable-next-line: no-string-literal
      this.store["reload"]();
  }

  public async ngOnChanges(changes: SimpleChanges) {
    // tslint:disable-next-line: curly
    if (this.store) {
      if (changes.modelId && changes.modelId.currentValue !== undefined) {
        let model: any;
        if (this.store instanceof PageableCrudStore)
          model = await this.store.get(this.modelId);
        else
          model = await this.store.getById(this.modelId);
        if (model) {
          this.value = model[this.property];
          if (this.value === undefined)
            this.value = this.modelId + "." + this.property + " is undefined!";
        }
        else {
          this.notFound = true;
          if (!this.notFoundTemplate && !this.notFoundText)
            this.notFoundText = this.modelId + " not found/loaded";
        }
        this.cdr.markForCheck();
      }
    }
  }
}
