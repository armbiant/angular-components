import { Component, Input, OnInit } from "@angular/core";
import { PageableStore, StoreState } from "@smallstack/store";


@Component({
  selector: "store-container",
  templateUrl: "./store-container.component.html",
  styleUrls: ["./store-container.component.scss"]
})
export class StoreContainerComponent implements OnInit {

  @Input()
  public store: PageableStore;

  @Input()
  public loadingText: string;

  public async ngOnInit() {
    if (this.store.state === StoreState.INITIAL && typeof this.store.load === "function")
      await this.store.load();
  }
}
