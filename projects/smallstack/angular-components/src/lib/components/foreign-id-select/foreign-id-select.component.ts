import { Component, EventEmitter, Input, Output } from "@angular/core";
import { MatSelectChange } from "@angular/material/select";
import { ObjectStore } from "@smallstack/store";

@Component({
  templateUrl: `./foreign-id-select.component.html`,
  selector: "foreign-id-select"
})
export class ForeignIdSelectComponent {

  @Input()
  public store: ObjectStore;

  @Input()
  public displayProperty: string = "id";

  @Input()
  public title: string = "Foreign Id";

  @Input()
  public multiple: boolean = false;

  @Input()
  public selected: string;

  @Output()
  public selectedChange: EventEmitter<string | string[]> = new EventEmitter();

  public selectionChange(selection: MatSelectChange) {
    this.selectedChange.emit(selection.value);
  }

}
