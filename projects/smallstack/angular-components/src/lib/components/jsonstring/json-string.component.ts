import { Pipe, PipeTransform } from "@angular/core";



@Pipe({
  name: "jsonstring"
})
export class JsonStringPipe implements PipeTransform {

  public transform(value: any) {
    if (value instanceof Array || typeof value === "object")
      return JSON.stringify(value, undefined, 4);
    return value;
  }

}
