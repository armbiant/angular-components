// tslint:disable: no-floating-promises

import { JsonStringPipe } from "./json-string.component";

describe("JsonString Pipe", () => {
  let pipe: JsonStringPipe;

  beforeEach(() => {
    pipe = new JsonStringPipe();
  });

  it("show string for string", () => {
    expect(pipe.transform("hallo")).toBe("hallo");
  });

  it("stringifies an object", () => {
    expect(pipe.transform({ huhu: 5 })).toBe(`{
    "huhu": 5
}`);
  });
});
