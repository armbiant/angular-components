import { Component, EventEmitter, Input, Output } from "@angular/core";


@Component({
  selector: "search-field",
  templateUrl: "./search-field.component.html",
  styleUrls: ["./search-field.component.scss"]
})
export class SearchFieldComponent {

  @Output()
  public search: EventEmitter<string> = new EventEmitter();

  @Input()
  public text: string;

  public doSearch() {
    this.search.emit(this.text);
  }

  public reset() {
    this.search.emit(undefined);
    this.text = undefined;
  }
}
