import { Component, Input } from "@angular/core";
import { currentLocale$ } from "../../services/locale.service";
import { ITranslationStore } from "../i18n/i18n.component";

@Component({
  selector: "ii18n,[ii18n]",
  templateUrl: "./ii18n.component.html"
})
export class II18nComponent {

  @Input()
  public translationStore: ITranslationStore;

  public key: string | any;
  public type: string;

  @Input("key")
  set _key(key: string | any) {
    // single @ fallback for now
    if (typeof key === "string" && key.startsWith("@") && !key.startsWith("@@"))
      key = "@" + key;

    if (typeof key === "string" && key.startsWith("@@")) {
      this.key = key.slice(2);
      this.type = "i18n";
    }
    else if (typeof key === "object" && typeof key.values === "object") {
      this.type = "inline";
      this.key = key;
    }
    else {
      this.type = "text";
      this.key = key;
    }
  }

  @Input()
  public replacers: any;

  @Input()
  public uppercase = false;

  @Input()
  public locale: string = currentLocale$.value;

}
