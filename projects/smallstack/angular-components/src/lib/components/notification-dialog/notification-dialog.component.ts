import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { NotifierButton } from "../../services/notification.service";

@Component({
  templateUrl: "./notification-dialog.component.html",
  styleUrls: ["./notification-dialog.component.scss"]
})
export class NotificationDialogComponent {
  public confirmMessage: string;

  constructor(public dialogRef: MatDialogRef<NotificationDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    if (data === undefined)
      data = {};
    if (data.buttons === undefined)
      data.buttons = [{ classes: "mat-flat-button mat-primary", label: "@@common.dismiss", value: true }] as NotifierButton[];
    else {
      const buttons: NotifierButton[] = data.buttons;
      data.buttons = buttons.map((btn) => {
        let classes = btn.classes;
        if (!classes)
          classes = "mat-flat-button" + btn.color ? " mat-" + btn.color : "";
        if (classes.indexOf("mat-flat-button") === -1)
          classes = "mat-flat-button " + classes;
        return {
          classes,
          label: btn.label,
          value: btn.value
        };
      });
    }
  }

}
