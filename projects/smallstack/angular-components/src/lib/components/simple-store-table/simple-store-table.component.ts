import { Component, ContentChild, EventEmitter, HostBinding, Input, Output, TemplateRef } from "@angular/core";
import { Observable } from "rxjs";

/**
 * @example
 * <simple-store-table [data]="workflowStore.currentPage$" [properties]="['id', 'label', 'description']">
 *   <ng-template #actions let-element="element">
 *     <button mat-flat-button (click)="trigger(element.id)">
 *       <i18n key="backoffice.workflows.executions.execute"></i18n>
 *     </button>
 *   </ng-template>
 * </simple-store-table>
 */

@Component({
  templateUrl: "./simple-store-table.component.html",
  selector: "simple-store-table",
  styleUrls: ["./simple-store-table.component.scss"]
})
export class SimpleStoreTableComponent {

  @Input()
  public data: any[] | Observable<any[]>;

  @Input()
  public properties: string[] = ["id"];

  @Output()
  public selected: EventEmitter<any> = new EventEmitter();

  @ContentChild("actions")
  public actions: TemplateRef<any>;

  get columns(): string[] {
    if (this.actions === undefined)
      return this.properties;
    else
      return this.properties.concat("actions");
  }

  @HostBinding("class")
  get hostClasses(): string {
    if (this.selected.observers.length > 0)
      return "rows-selectable";
  }
  public select(elem: any) {
    this.selected.emit(elem);
  }

}
