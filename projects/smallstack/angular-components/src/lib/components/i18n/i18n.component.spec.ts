// tslint:disable: no-floating-promises
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { CloudApiClient } from "@smallstack/cloud-api-client";
import { AngularComponentsModule } from "../../angular-components.module";
import { LocaleService } from "../../services/locale.service";
import { TranslationService } from "../../services/translation.service";
import { I18nComponent } from "./i18n.component";

describe("I18n", () => {
  let component: I18nComponent;
  let fixture: ComponentFixture<I18nComponent>;
  let translationService: TranslationService;
  let localeService: LocaleService;

  beforeEach(async(async () => {
    await TestBed.configureTestingModule({
      imports: [AngularComponentsModule],
      providers: [
        TranslationService,
        LocaleService,
        {
          provide: CloudApiClient,
          useFactory: () => {
            return {};
          }
        }
      ],
      declarations: [I18nComponent]
    }).compileComponents();
    translationService = TestBed.get(TranslationService);
    localeService = TestBed.get(LocaleService);
    localeService.setCurrentLocale("de_de");
    fixture = TestBed.createComponent(I18nComponent);
    component = fixture.componentInstance;
  }));

  it("show nothing for nothing", async () => {
    expect(component.value).toBe(undefined);
  });

  it("show simple translated string", async () => {
    translationService.addTranslation("de_de", { "hello": "Hallo" });
    component.key = "hello";
    fixture.detectChanges();
    expect(component.value).toBe("Hallo");
  });

  it("show translated string with replacer", async () => {
    translationService.addTranslation("de_de", { "hello": "Hallo ${name}" });
    component.key = "hello";
    fixture.detectChanges();
    expect(component.value).toBe("Hallo ${name}");
    component.replacers = { name: "Otto" };
    component.translate();
    fixture.detectChanges();
    expect(component.value).toBe("Hallo Otto");
    localeService.setCurrentLocale("en_us");
    await component.translate();
    expect(component.value).toBe("_en_us:hello_");
  });

  it("show translated string based on fallback chain", async () => {
    translationService.addTranslation("de_de", { "hello": "Hallo ${name}" });
    localeService.addFallbackChain("en_us", ["en_us", "de_de"]);
    component.key = "hello";
    fixture.detectChanges();
    expect(component.value).toBe("Hallo ${name}");
    component.replacers = { name: "Otto" };
    await component.translate();
    fixture.detectChanges();
    expect(component.value).toBe("Hallo Otto");
    localeService.setCurrentLocale("en_us");
    await component.translate();
    expect(component.value).toBe("Hallo Otto");
  });
});
