import { ChangeDetectionStrategy, ChangeDetectorRef, Component, HostListener, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from "@angular/core";
import { Translation } from "@smallstack/cloud-api-client";
import { Logger } from "@smallstack/core-common";
import { PageableCrudStore } from "@smallstack/store";
import { Subscription } from "rxjs";
import { currentLocale$, LocaleService } from "../../services/locale.service";
import { TranslationService } from "../../services/translation.service";

export interface ITranslationStore extends PageableCrudStore {
  getByKey(key: string): Promise<Translation>;
}

@Component({
  selector: "i18n",
  templateUrl: "./i18n.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class I18nComponent implements OnInit, OnDestroy, OnChanges {

  @Input()
  public translationStore: ITranslationStore;

  @Input()
  public key: string;

  @Input()
  public replacers: any;

  @Input()
  public i18nTexts: { [lang: string]: string };

  @Input()
  public uppercase = false;

  @Input()
  public showMissingKey = true;

  @Input()
  public defaultValue: string;

  @Input()
  public locale: string;

  public value: string;

  public tooltipEnabled = false;

  private subscription: Subscription;

  constructor(private cdr: ChangeDetectorRef,
    private translationService: TranslationService,
    private localeService: LocaleService) {
  }

  public async ngOnInit() {
    this.subscription = currentLocale$.subscribe(async (locale: string) => {
      Logger.debug("I18nComponent", `Got locale change to : ${locale}`);
      await this.translate();
    });
    await this.translate();
  }

  public ngOnDestroy(): void {
    if (this.subscription)
      this.subscription.unsubscribe();
  }

  @HostListener("mouseover", ["$event"])
  public onHover(e: MouseEvent) {
    if (e.shiftKey) {
      Logger.info("I18nComponent", "================ I18N DEBUG ================");
      Logger.info("BaseEditor", " |-- Key:             " + this.key);
      Logger.info("BaseEditor", " |-- Replacers:       " + JSON.stringify(this.replacers));
      Logger.info("BaseEditor", " |-- I18nTexts:       " + JSON.stringify(this.i18nTexts));
      Logger.info("BaseEditor", " |-- ShowMissingKey:  " + JSON.stringify(this.showMissingKey));
    }
  }

  public async ngOnChanges(changes: SimpleChanges) {
    const { key } = changes;

    if (key && key.currentValue !== key.previousValue)
      await this.translate();
  }

  public async translate() {
    const options = { showMissingKey: this.showMissingKey, replacers: this.replacers, locale: this.locale };
    // tslint:disable-next-line: curly
    if (this.key) {
      // if local translationStore is set
      if (this.translationStore) {
        if (typeof this.translationStore.getByKey !== "function")
          throw new Error("getByKey is not defined in translationStore!");
        const translation: Translation = await this.translationStore.getByKey(this.key);
        const translatedString: string = translation?.values[this.locale];
        // tslint:disable-next-line: curly
        if (!translation || !translatedString) {
          if (this.showMissingKey)
            this.value = "_" + this.locale + ":" + this.key + "_";
          else
            this.value = "";
        }
        // tslint:disable-next-line: curly
        else {
          if (translatedString !== undefined)
            this.value = this.uppercase ? translatedString.toUpperCase() : translatedString;
          else if (this.defaultValue)
            this.value = this.uppercase ? this.defaultValue.toUpperCase() : this.defaultValue;
        }
      }
      // if no store is present, ask translationService
      else {
        const translatedString: string = this.translationService.getTranslation(this.key.toLowerCase(), options);
        if (translatedString !== undefined)
          this.value = this.uppercase ? translatedString.toUpperCase() : translatedString;
        else
          if (this.defaultValue)
            this.value = this.uppercase ? this.defaultValue.toUpperCase() : this.defaultValue;
          else
            this.value = this.translationService.getTranslation(this.key.toLowerCase(), options);
      }
    } else if (this.i18nTexts) {
      const langKey: string = this.localeService.getCurrentLocale();
      if (this.i18nTexts[langKey] !== undefined)
        this.value = this.i18nTexts[langKey];
      else {
        const defaultLanguage: string = await this.localeService.getDefaultLocale();
        if (this.i18nTexts[defaultLanguage] !== undefined)
          this.value = this.i18nTexts[defaultLanguage];
        else
          this.value = `_missing translation for language ${langKey} and default language ${defaultLanguage}`;
      }
    }
    this.cdr.detectChanges();
  }
}
