import { Component, Input, OnInit } from "@angular/core";
import { CloudApiClient } from "@smallstack/cloud-api-client";
import jwtDecode from "jwt-decode";


@Component({
  selector: "verification-message",
  templateUrl: "./verification-message.component.html",
  styleUrls: ["./verification-message.component.scss"]
})
export class VerificationMessageComponent implements OnInit {

  @Input()
  private token: string;
  public loading = true;
  public emailConfirmed = false;
  public userEmailAddress: string;
  public errorMessage: string;
  private cloudApiClient: CloudApiClient;

  ngOnInit(): void {
    if (this.token) {
      try {
        const decoded: any = jwtDecode(this.token);
        if (!decoded.resellerId || !decoded.tenantId || !decoded.baseUrl)
          throw new Error("Invalid token payload");
        this.cloudApiClient = new CloudApiClient({
          baseUrl: decoded.baseUrl as string,
          resellerIdProvider: decoded.resellerId as string,
          tenantIdProvider: decoded.tenantId as string
        });
      } catch (e) {
        this.loading = false;
        this.errorMessage = "@@users.verify.email.error.token.invalid";
      }
      this.cloudApiClient.confirmEmail(this.token).then(() => {
        this.loading = false;
        this.emailConfirmed = true;
      }).catch((e) => {
        this.loading = false;
        switch (e.statusCode) {
          case 401:
            this.errorMessage = "@@users.verify.email.error.token.expired";
            break;
          case 400:
            this.errorMessage = "@@users.verify.email.error.token.invalid";
            break;
          case 404:
            this.errorMessage = "@@users.verify.email.error.usernotfound";
            break;
          default:
            this.errorMessage = e.message;
        }
      });
    }
    else {
      this.loading = false;
      this.errorMessage = "@@users.verify.email.error.token.notprovided";
    }
  }

}
