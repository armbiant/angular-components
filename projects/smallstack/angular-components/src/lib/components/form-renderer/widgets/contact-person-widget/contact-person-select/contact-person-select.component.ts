import { Component, EventEmitter, Inject, Input, OnDestroy, Optional, Output } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { BehaviorSubject, Subscription } from "rxjs";
import { debounceTime, tap } from "rxjs/operators";
import { CheckinContactPerson } from "../../../../../stores";


@Component({
  selector: "contact-person-select",
  templateUrl: "./contact-person-select.component.html",
  styleUrls: ["./contact-person-select.component.scss"]
})
export class ContactPersonSelectComponent implements OnDestroy {

  @Input()
  public contactPersons: CheckinContactPerson[];

  @Output()
  public selectedContactPerson: EventEmitter<CheckinContactPerson> = new EventEmitter();

  public filteredContactPersons: CheckinContactPerson[];

  private searchText$: BehaviorSubject<string> = new BehaviorSubject<string>("");
  private sub: Subscription;

  constructor(@Inject(MAT_DIALOG_DATA) @Optional() data: any, @Optional() private dialogRef: MatDialogRef<any>) {
    if (data?.contactPersons)
      this.contactPersons = data.contactPersons;

    this.filteredContactPersons = this.contactPersons;
    this.sub = this.searchText$.pipe(
      debounceTime(300),
      tap((searchText) => {
        // tslint:disable-next-line: curly
        if (typeof searchText === "string") {
          this.filteredContactPersons = this.contactPersons.filter((cp) => {
            if (cp.firstName?.toLowerCase().includes(searchText.toLowerCase()))
              return true;
            if (cp.lastName?.toLowerCase().includes(searchText.toLowerCase()))
              return true;
            if (cp.email?.toLowerCase().includes(searchText.toLowerCase()))
              return true;
            return false;
          });
        }
        else
          this.filteredContactPersons = this.contactPersons;
      })
    ).subscribe();
  }

  public ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  public selectContactPerson(person: CheckinContactPerson) {
    this.selectedContactPerson.emit(person);
    if (this.dialogRef)
      this.dialogRef.close(person);
  }

  public searchChanged(searchChanged: any) {
    this.searchText$.next(searchChanged.srcElement.value);
  }

  public clearSearch() {
    this.searchText$.next("");
  }
}
