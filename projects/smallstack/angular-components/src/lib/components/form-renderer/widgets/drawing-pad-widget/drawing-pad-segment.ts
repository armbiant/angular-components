export interface DrawingPadSegment {
  x: number;
  y: number;
  time: number;
  color: string;
}

export type signatureData = DrawingPadSegment[][];
