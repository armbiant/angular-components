import { JsonSchemaFormService } from "@ajsf/core";
import { ChangeDetectorRef, Component, Input, OnInit } from "@angular/core";
import { AbstractControl } from "@angular/forms";
import { BehaviorSubject } from "rxjs";

@Component({
  templateUrl: "./imageupload-widget.component.html",
  styleUrls: ["./imageupload-widget.component.scss"]
})
export class ImageUploadWidgetComponent implements OnInit {

  @Input() public layoutNode: any;
  public formControl: AbstractControl;

  public isLoading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public error: any;
  public fileName: string;
  public propertyTitle: string;
  public file: File;
  public fileSrc: any;

  constructor(private jsf: JsonSchemaFormService, private cdr: ChangeDetectorRef) { }

  public ngOnInit() {
    this.jsf.initializeControl(this);
    this.propertyTitle = this.layoutNode.options.title;
  }

  public onFileSelect(event: any) {
    this.isLoading$.next(true);
    if (!this.jsf.data.files)
      this.jsf.data.files = {};
    if (event.target.files.length > 0) {
      this.file = event.target.files[0];
      this.fileName = this.file.name;
      this.isLoading$.next(false);
      const reader = new FileReader();
      reader.readAsDataURL(this.file);
      reader.onload = () => {
        this.fileSrc = reader.result;
        this.jsf.updateValue(this, reader.result);
        this.cdr.markForCheck();
      };
      reader.onerror = (error) => {
        this.error = "Error: " + error;
        this.isLoading$.next(false);
        this.cdr.markForCheck();
        throw error;
      };
    }
  }
}
