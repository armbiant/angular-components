import { JsonSchemaFormService } from "@ajsf/core";
import { ChangeDetectorRef, Component, Input, OnInit } from "@angular/core";
import { AbstractControl } from "@angular/forms";
import { MatCheckboxChange } from "@angular/material/checkbox";
import { MatDialog } from "@angular/material/dialog";
import { LinkViewDialogComponent } from "./link-view-dialog/link-view-dialog.component";

@Component({
  templateUrl: "./document-widget.component.html",
  styleUrls: ["./document-widget.component.scss"]
})
export class DocumentWidgetComponent implements OnInit {

  @Input() public layoutNode: any;

  public checked: boolean = false;
  public propertyTitle: string;
  public fileUrl: string;
  public formControl: AbstractControl;

  constructor(private jsf: JsonSchemaFormService, private dialog: MatDialog, private cdr: ChangeDetectorRef) { }

  public ngOnInit(): void {
    this.jsf.initializeControl(this);
    this.propertyTitle = this.layoutNode.options.title;
    this.fileUrl = this.layoutNode.options.fileUrl;
  }

  public openFilePreview() {
    this.dialog.open(LinkViewDialogComponent, {
      data: { src: this.fileUrl, title: this.propertyTitle },
      panelClass: "fullscreen-dialog",
      autoFocus: false,
      height: "100vh",
      minWidth: "50vh"
    }).afterClosed().subscribe((result) => {
      const { accepted } = result;
      this.checked = accepted;
      if (accepted)
        this.jsf.updateValue(this, accepted);
      else
        this.jsf.updateValue(this, undefined);
      this.cdr.markForCheck();
    });
  }

  public markValue($event: MatCheckboxChange) {
    if ($event.checked)
      this.jsf.updateValue(this, $event.checked);
    else
      this.jsf.updateValue(this, undefined);

    this.checked = $event.checked;
  }


}
