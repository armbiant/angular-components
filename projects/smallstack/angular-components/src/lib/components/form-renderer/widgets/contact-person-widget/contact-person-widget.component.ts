import { JsonSchemaFormService } from "@ajsf/core";
import { Component, Input, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { StoreState } from "@smallstack/store";
import { CheckinContactPerson, CheckinContactPersonStore } from "../../../../stores";
import { ContactPersonSelectComponent } from "./contact-person-select/contact-person-select.component";

@Component({
  templateUrl: "./contact-person-widget.component.html",
  styleUrls: ["./contact-person-widget.component.scss"]
})
export class ContactPersonWidgetComponent implements OnInit {

  public selectedContactPerson: CheckinContactPerson;
  public propertyTitle: string;

  @Input() public layoutNode: any;

  public formControl: FormControl;

  constructor(private dialog: MatDialog, private jsf: JsonSchemaFormService, private checkinContactPersonStore: CheckinContactPersonStore) { }

  public async ngOnInit() {
    this.jsf.initializeControl(this);
    this.propertyTitle = this.layoutNode.options.title;
    if (this.checkinContactPersonStore.state === StoreState.INITIAL)
      await this.checkinContactPersonStore.preload();
  }

  public openContactPersonSelect() {
    const mappedIds: string[] = this.layoutNode.options.contactPersons || [];
    const contactPersons = this.checkinContactPersonStore.value.filter((person) => mappedIds.includes(person.id));
    this.dialog.open(ContactPersonSelectComponent, { width: "100%", height: "100%", disableClose: false, panelClass: "fullscreen-dialog", autoFocus: false, data: { contactPersons } }).afterClosed().subscribe((contactPerson: CheckinContactPerson) => {
      this.jsf.updateValue(this, contactPerson?.id);
      this.selectedContactPerson = contactPerson;
    });
  }
}
