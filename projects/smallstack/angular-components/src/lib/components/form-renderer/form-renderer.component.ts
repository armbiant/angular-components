import { JsonSchemaFormComponent, WidgetLibraryService } from "@ajsf/core";
import { AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { Subject } from "rxjs";
import { Form } from "./form.type";
import { ContactPersonWidgetComponent } from "./widgets/contact-person-widget/contact-person-widget.component";
import { DocumentWidgetComponent } from "./widgets/document-widget/document-widget.component";
import { DrawingPadWidgetComponent } from "./widgets/drawing-pad-widget/drawing-pad-widget.component";
import { ImageUploadWidgetComponent } from "./widgets/imageupload-widget/imageupload-widget.component";


@Component({
  selector: "form-renderer",
  templateUrl: "./form-renderer.component.html",
  styleUrls: ["./form-renderer.component.scss"]
})
export class FormRendererComponent implements AfterViewInit {

  @Input()
  public form: Form;

  @Input()
  public validationErrorsText: string;

  @Input()
  public validation: boolean = true;

  @Input()
  public formData: any = {};

  @Input()
  public showSubmitButton: boolean = true;

  @Output()
  public formDataChange: EventEmitter<any> = new EventEmitter();

  @ViewChild(JsonSchemaFormComponent, { static: false, read: ElementRef })
  public jsonSchemaFormComponentElementRef: ElementRef;

  @ViewChild(JsonSchemaFormComponent)
  public jsonSchemaFormComponent: JsonSchemaFormComponent;

  public formInputDataIsValid: boolean;
  public validationErrors: any;

  public onceChecked: boolean = false;

  public jsonFormOptions = {
    autocomplete: false
  };

  /** Subject that emits when the component has been destroyed. */
  protected onDestroy = new Subject<void>();

  constructor(widgetLibraryService: WidgetLibraryService) {
    widgetLibraryService.registerWidget("imageupload", ImageUploadWidgetComponent);
    widgetLibraryService.registerWidget("contactperson", ContactPersonWidgetComponent);
    widgetLibraryService.registerWidget("drawingpad", DrawingPadWidgetComponent);
    widgetLibraryService.registerWidget("document", DocumentWidgetComponent);
  }

  public ngAfterViewInit(): void {
    const inputs = this.jsonSchemaFormComponentElementRef.nativeElement.querySelectorAll("input");
    inputs.forEach((input: HTMLInputElement) => {
      input.autocomplete = "off";
    });
  }

  public validate() {
    this.onceChecked = true;
    const formGroup: FormGroup = this.jsonSchemaFormComponent.jsf.formGroup;
    formGroup.markAllAsTouched();
    this.jsonSchemaFormComponent.jsf.validateData(formGroup.value);
  }

  public async sendFormData() {
    if (this.validationErrors && this.validation)
      this.validate();
    else
      this.formDataChange.emit(this.formData);
  }

}
