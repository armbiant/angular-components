import { JSONSchema7 } from "json-schema";

export interface Form {
  id: string;
  title: string;
  fields: FormField;
  layout: FormLayoutRow[];
  submitButtonText: string;
}

export interface FormField extends JSONSchema7 {
  "x-schema-form"?: {
    [key: string]: any;
    type?: string;
  };
}

export interface FormLayoutRow {
  "type": string;
  "title"?: string;
  "flex-flow"?: string;
  "items"?: FormLayoutItem[];
}

export interface FormLayoutItem {
  key: string;
  appearance: string;
  floatLabel: string;
}
