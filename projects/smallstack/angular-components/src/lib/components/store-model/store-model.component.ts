import { ChangeDetectorRef, Component, ContentChild, Input, OnChanges, SimpleChanges, TemplateRef } from "@angular/core";
import { ObjectStore, PageableCrudStore } from "@smallstack/store";


/**
 * Displays store data within a template.
 * Usage: https://gitlab.com/smallstack/projects/cloud-backoffice/snippets/1954603
 */
@Component({
  selector: "store-model",
  templateUrl: "./store-model.component.html",
  styleUrls: ["./store-model.component.scss"]
})
export class StoreModelComponent implements OnChanges {

  @Input()
  public store: ObjectStore | PageableCrudStore;

  @Input()
  public modelId: string;

  @Input()
  public notFound: string;

  public model: any;
  public notFoundMsg: string;
  @ContentChild(TemplateRef)
  public templateModel: TemplateRef<any>;

  constructor(private cdr: ChangeDetectorRef) { }

  public async ngOnChanges(changes: SimpleChanges) {
    // tslint:disable-next-line: curly
    if (this.store) {
      if (changes.modelId) {
        if (this.modelId)
          if (this.store instanceof PageableCrudStore)
            this.model = await this.store.get(this.modelId);
          else
            this.model = await this.store.getById(this.modelId);
        if (!this.model)
          if (this.notFound)
            this.notFoundMsg = this.notFound;
          else
            this.notFoundMsg = `Id ${this.modelId} not found/loaded`;
        this.cdr.markForCheck();
      }
    }
  }
}
