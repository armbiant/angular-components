import { ChangeDetectionStrategy, Component } from "@angular/core";
import { BaseWidget } from "../../schema-form-widget/base-widget.component";

@Component({
  selector: "stringselect-widget",
  templateUrl: "./stringselect-widget.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StringSelectWidgetComponent extends BaseWidget { }
