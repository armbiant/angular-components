import { ChangeDetectionStrategy, Component } from "@angular/core";
import { BaseWidget } from "../../schema-form-widget/base-widget.component";

@Component({
  selector: "number-widget",
  templateUrl: "./number-widget.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NumberWidgetComponent extends BaseWidget {

}
