import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { BaseWidget } from "../../schema-form-widget/base-widget.component";

@Component({
  selector: "input-widget",
  templateUrl: "./input-widget.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputWidgetComponent extends BaseWidget { }
