import { ChangeDetectionStrategy, Component } from "@angular/core";
import { BaseWidget } from "../../schema-form-widget/base-widget.component";

@Component({
  selector: "checkbox-widget",
  templateUrl: "./checkbox-widget.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckboxWidgetComponent extends BaseWidget {

}
