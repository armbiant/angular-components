import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { SearchByField, SearchByFieldMatcher, SQBuilder } from "@smallstack/cloud-api-client";
import { applicableMatchers } from "../../../store-search/store-search.component";
import { BaseWidget } from "../../schema-form-widget/base-widget.component";


export interface SearchQueryWidgetField extends SearchByField {
  type: string;
}

@Component({
  templateUrl: "./search-query-widget.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ["./search-query-widget.component.scss"]
})
export class SearchQueryWidgetComponent extends BaseWidget implements OnInit {


  public fields: SearchQueryWidgetField[] = [];
  public applicableMatchers = applicableMatchers;

  public ngOnInit(): void {
    if (this.data.fieldSearches instanceof Array)
      this.fields = this.data.fieldSearches;
  }

  public addNewField() {
    this.fields.push({
      fieldname: "",
      type: "string",
      value: "",
      caseSensitive: true,
      matcher: SearchByFieldMatcher.EXACT_MATCH
    });
  }

  public changeField(field: SearchQueryWidgetField, property: string, value: any) {
    field[property] = value;
    if (property === "type") {
      field.value = undefined;
      field.matcher = undefined;
    }
    this.dataChanged();
  }

  public removeField(i: number) {
    this.fields.splice(i, 1);
  }

  public dataChanged() {
    this.dataChange.emit(SQBuilder.asSearchQuery(this.fields));
  }

}
