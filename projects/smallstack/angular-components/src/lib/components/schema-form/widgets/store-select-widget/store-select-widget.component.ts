import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { Store } from "@smallstack/store";
import { StoreRegistry } from "../../../../stores/store.registry";
import { BaseWidget } from "../../schema-form-widget/base-widget.component";

@Component({
  templateUrl: "./store-select-widget.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StoreSelectWidgetComponent extends BaseWidget implements OnInit {
  public store: Store;
  public storeName: string;
  public propertyName: string = "id";
  public multiple: boolean = false;

  constructor(private storeRegistry: StoreRegistry) {
    super();

  }

  public ngOnInit(): void {
    this.storeName = this.schema["x-schema-form"].store;
    if (!this.storeName)
      throw new Error("no schema.x-schema-form.store set!");
    if (this.schema["x-schema-form"].propertyName)
      this.propertyName = this.schema["x-schema-form"].propertyName;
    if (this.schema["x-schema-form"].multiple)
      this.multiple = this.schema["x-schema-form"].multiple;
    this.store = this.storeRegistry.getStore(this.storeName);
    if (!this.store)
      throw new Error("no store found by name: " + this.storeName);
  }

  public dataChanged(data: string) {
    this.dataChange.emit(data);
    this.data = data;
  }
}
