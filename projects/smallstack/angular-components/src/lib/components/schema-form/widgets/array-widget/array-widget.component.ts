import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { JSONSchema7 } from "json-schema";
import { BaseWidget } from "../../schema-form-widget/base-widget.component";

@Component({
  templateUrl: "./array-widget.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ArrayWidgetComponent extends BaseWidget implements OnInit {

  public ngOnInit(): void {
    // add default schema for array items
    if (this.schema.items === undefined)
      this.schema.items = {
        type: "string"
      };
    if (this.options === undefined)
      this.options = {};
    this.options.fxLayout = "row";
  }

  public dataChangedFromChild(data: any, index: number) {
    this.data[index] = data;
    this.dataChange.emit(this.data);
  }

  public addEntry() {
    if (this.data === undefined)
      this.data = [];
    const arraySchema: JSONSchema7 = this.schema.items as JSONSchema7;
    if (arraySchema.type === "string")
      this.data.push("");
    else if (arraySchema.type === "number" || arraySchema.type === "integer")
      this.data.push(0);
    else if (arraySchema.type === "boolean")
      this.data.push(true);
    else this.data.push({});
  }

  public removeEntry(index: number) {
    if (this.data !== undefined) {
      (this.data as any[]).splice(index, 1);
      this.dataChange.emit(this.data);
    }
  }

  public trackByFn(index: number, item: any) {
    return index;
  }
}
