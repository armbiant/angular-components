import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { BaseWidget, WidgetOptions } from "../../schema-form-widget/base-widget.component";

export interface ObjectWidgetOptions extends WidgetOptions {
  fxLayout?: string;
}



@Component({
  templateUrl: "./object-widget.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ObjectWidgetComponent extends BaseWidget implements OnInit {

  public fxLayout: string = "column";

  public ngOnInit(): void {
    if (this.options.fxLayout)
      this.fxLayout = this.options.fxLayout;
  }

  public dataChangedFromChild(subKey: string, data: any) {
    this.data[subKey] = data;
    this.dataChange.emit(this.data);
  }
}
