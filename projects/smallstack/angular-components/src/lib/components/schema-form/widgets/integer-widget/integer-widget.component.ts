import { ChangeDetectionStrategy, Component } from "@angular/core";
import { BaseWidget } from "../../schema-form-widget/base-widget.component";

@Component({
  selector: "integer-widget",
  templateUrl: "./integer-widget.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IntegerWidgetComponent extends BaseWidget {

}
