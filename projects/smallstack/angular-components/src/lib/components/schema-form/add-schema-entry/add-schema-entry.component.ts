import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material/dialog";
import { SchemaFormSchema } from "../schema-form-schema";

@Component({
  templateUrl: "./add-schema-entry.component.html"
})
export class AddSchemaEntryComponent {

  public schema: SchemaFormSchema = { type: "string" };
  public key: string;

  constructor(private matDialogRef: MatDialogRef<any>) { }

  public addProperty() {
    this.schema.title = this.key;
    if (this.schema.type === undefined)
      this.schema.type = "string";
    this.schema["x-schema-form"] = { dataDrivenSchema: true };
    this.matDialogRef.close({ key: this.key, schema: this.schema });
  }

}
