import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ContentChild, OnInit, TemplateRef } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { AddSchemaEntryComponent } from "../add-schema-entry/add-schema-entry.component";
import { SchemaFormSchema } from "../schema-form-schema";
import { BaseWidget } from "../schema-form-widget/base-widget.component";

@Component({
  selector: "schema-form-table",
  templateUrl: "./schema-form-table.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SchemaFormTableComponent extends BaseWidget implements OnInit {

  @ContentChild("actions")
  public actions: TemplateRef<any>;

  public columns: string[] = ["path", "value", "actions"];

  constructor(private matDialog: MatDialog, private cdr: ChangeDetectorRef) {
    super();
    if (this.options === undefined)
      this.options = {};
    this.options.showLabel = false;
  }

  public ngOnInit(): void {
    // check for data elements which aren't in the schema
    if (this.data)
      for (const key in this.data)
        if (this.data[key] && this.schema?.properties?.[key] === undefined) {
          const newEntry: SchemaFormSchema = {
            type: typeof this.data[key] as any,
            title: key,
            "x-schema-form": {
              dataDrivenSchema: true
            }
          };
          if (this.schema.properties === undefined)
            this.schema.properties = {};
          this.schema.properties[key] = newEntry;
        }
  }

  public dataChanged(key: string, data: any) {
    if (this.data === undefined)
      this.data = {};
    this.data[key] = data;
    this.dataChange.emit(this.data);
  }

  public openAddSchemaDialog() {
    this.matDialog.open(AddSchemaEntryComponent).afterClosed().subscribe((data: { key: string, schema: SchemaFormSchema }) => {
      if (data?.key !== undefined && data?.key !== "") {
        if (this.schema.properties === undefined)
          this.schema.properties = {};
        this.schema.properties[data.key] = data.schema;
        this.cdr.markForCheck();
      }
    });
  }

}
