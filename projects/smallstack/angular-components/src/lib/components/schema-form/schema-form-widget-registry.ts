import { Injectable } from "@angular/core";
import { Logger } from "@smallstack/core-common";
import { SchemaFormSchema } from "./schema-form-schema";
import { BaseWidget } from "./schema-form-widget/base-widget.component";
import { ArrayWidgetComponent } from "./widgets/array-widget/array-widget.component";
import { CheckboxWidgetComponent } from "./widgets/checkbox-widget/checkbox-widget.component";
import { InputWidgetComponent } from "./widgets/input-widget/input-widget.component";
import { IntegerWidgetComponent } from "./widgets/integer-widget/integer-widget.component";
import { NumberWidgetComponent } from "./widgets/number-widget/number-widget.component";
import { ObjectWidgetComponent } from "./widgets/object-widget/object-widget.component";
import { SearchQueryWidgetComponent } from "./widgets/search-query-widget/search-query-widget.component";
import { StoreSelectWidgetComponent } from "./widgets/store-select-widget/store-select-widget.component";
import { StringSelectWidgetComponent } from "./widgets/stringselect-widget/stringselect-widget.component";



/**
 * Holds UI Widget Representations used by the Schema Form to render JSON Schema Forms
 */
@Injectable({ "providedIn": "root" })
export class SchemaFormWidgetsRegistry {

  private registry: { [name: string]: typeof BaseWidget } = {};

  constructor() {
    this.addWidget("string", InputWidgetComponent);
    this.addWidget("stringselect", StringSelectWidgetComponent);
    this.addWidget("boolean", CheckboxWidgetComponent);
    this.addWidget("number", NumberWidgetComponent);
    this.addWidget("integer", IntegerWidgetComponent);
    this.addWidget("storeselect", StoreSelectWidgetComponent as any);
    this.addWidget("searchquery", SearchQueryWidgetComponent);
    this.addWidget("object", ObjectWidgetComponent);
    this.addWidget("array", ArrayWidgetComponent);
  }

  /**
   * Adds (or overwrites) a widget for the given name
   * @param name can be a primitive json schema type like boolean, number etc. or a custom one like 'my-widget'. You can explicitly set the widget in your schema via `schema.x-schema-form.widget` or let this registry try to find the correct one
   * @param component An angular component based on the {@link BaseWidget} classs
   */
  public addWidget(name: string, component: typeof BaseWidget): void {
    this.registry[name] = component;
  }

  public getWidgetByName(name: string): typeof BaseWidget {
    if (this.registry[name])
      return this.registry[name];
    Logger.error("SchemaFormWidgetsRegistry", "No widget found for name: " + name);
  }

  /**
   * This method determines, based on the schema, which widget to use
   */
  public getWidgetBySchema(schema: SchemaFormSchema): typeof BaseWidget {
    if (!schema)
      return undefined;

    // if widget is explicitly set, use that one
    if (schema["x-schema-form"]?.widget)
      return this.getWidgetByName(schema["x-schema-form"].widget);

    // string enum
    if (schema.enum !== undefined && (schema.type === "string" || schema.type === undefined))
      return this.getWidgetByName("stringselect");

    // try schema.type
    return this.getWidgetByName(schema.type as string);
  }

}
