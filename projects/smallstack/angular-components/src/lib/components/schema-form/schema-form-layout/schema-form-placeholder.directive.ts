import { Directive, Input, ViewContainerRef } from "@angular/core";

@Directive({
  selector: "[schemaFormPlaceholder]"
})
export class SchemaFormPlaceholderDirective {

  @Input()
  public schemaFormPlaceholder: string;

  constructor(public viewContainerRef: ViewContainerRef) { }

}
