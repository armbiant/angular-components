import { AfterViewInit, ChangeDetectionStrategy, Component, ComponentFactoryResolver, ContentChildren, QueryList } from "@angular/core";
import { FormField } from "../../form-renderer";
import { BaseWidget } from "../schema-form-widget/base-widget.component";
import { SchemaFormWidgetComponent } from "../schema-form-widget/schema-form-widget.component";
import { SchemaFormPlaceholderDirective } from "./schema-form-placeholder.directive";

@Component({
  selector: "schema-form-layout",
  templateUrl: "./schema-form-layout.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SchemaFormLayoutComponent extends BaseWidget implements AfterViewInit {

  @ContentChildren(SchemaFormPlaceholderDirective, { descendants: true })
  public schemaFormWidgets: QueryList<SchemaFormPlaceholderDirective>;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {
    super();
  }

  public ngAfterViewInit(): void {
    this.schemaFormWidgets.forEach((widget: SchemaFormPlaceholderDirective) => {
      widget.viewContainerRef.clear();
      const createdComponent = widget.viewContainerRef.createComponent<SchemaFormWidgetComponent>(this.componentFactoryResolver.resolveComponentFactory(SchemaFormWidgetComponent));
      if (!this.schema || !this.schema.properties)
        throw new Error("Please provide a schema with properties for <schema-form-layout schema=\"...\">");
      if (!this.schema.properties[widget.schemaFormPlaceholder])
        throw new Error("Please provide a schema for property '" + widget.schemaFormPlaceholder + "' in <schema-form-layout schema=\"...\">");
      createdComponent.instance.schema = this.schema.properties[widget.schemaFormPlaceholder] as FormField;
      createdComponent.instance.data = this.data?.[widget.schemaFormPlaceholder];
      createdComponent.instance.name = widget.schemaFormPlaceholder;
      createdComponent.instance.options = this.options;
      createdComponent.instance.dataChange.subscribe((d: any) => {
        if (this.data === undefined)
          this.data = {};
        this.data[widget.schemaFormPlaceholder] = d;
        this.dataChange.emit(this.data);
      });
    });
  }

}
