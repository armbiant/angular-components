import { JSONSchema7 } from "json-schema";

export interface SchemaFormSchema extends JSONSchema7 {
  "x-schema-form"?: {
    [key: string]: any;

    /**
     * sets the widget for this schema explicitly
     */
    widget?: string;

    /**
     * if e.g. the schema-form-table got data that is not represented in the schema, it will automatically create schema entries. Those entries will have this flag set to true
     */
    dataDrivenSchema?: boolean;
  };
}
