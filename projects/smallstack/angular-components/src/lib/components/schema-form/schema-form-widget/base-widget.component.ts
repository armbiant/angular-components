import { Directive, EventEmitter, HostListener, Input, Output } from "@angular/core";
import { Logger, Utils } from "@smallstack/core-common";
import { SchemaFormSchema } from "../schema-form-schema";

export interface WidgetOptions {
  showLabel?: boolean;
  appearance?: string;
}

@Directive()
export class BaseWidget {

  @Input()
  public name: string;

  @Input("schema")
  public schema: SchemaFormSchema;

  @Input()
  public data: any = {};

  @Input()
  public options: WidgetOptions | any = { showLabel: true };

  @Output()
  public dataChange: EventEmitter<any> = new EventEmitter();


  @HostListener("click", ["$event"])
  public onHover(e: MouseEvent) {
    if (e.shiftKey) {
      e.stopImmediatePropagation();
      e.preventDefault();
      Logger.info("BaseWidget", "================ BaseWidget DEBUG ================");
      Logger.info("BaseWidget", " |-- Name:    " + this.name);
      Logger.info("BaseWidget", " |-- Schema:  " + JSON.stringify(this.schema));
      Logger.info("BaseWidget", " |-- Data:    " + JSON.stringify(this.data));
      Logger.info("BaseWidget", " |-- Options: " + JSON.stringify(this.options));
    }
  }

  public getLabel() {
    if (this.schema.title)
      return this.schema.title;
    return Utils.capitalize(this.name);
  }

}
