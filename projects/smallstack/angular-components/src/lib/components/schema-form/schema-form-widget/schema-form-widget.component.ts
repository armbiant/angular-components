import { ChangeDetectionStrategy, Component, ComponentFactoryResolver, OnChanges, OnInit, SimpleChanges, ViewChild, ViewContainerRef } from "@angular/core";
import { Logger } from "@smallstack/core-common";
import { SchemaFormWidgetsRegistry } from "../schema-form-widget-registry";
import { BaseWidget } from "./base-widget.component";



/**
 * A schema form widget renders a json schema form. It delegates UI rendering to widgets based on the schema form type & schema properties
 *
 * @example
 * <schema-form-widget [schema]="myJsonSchema" [options]="{showLabel: true}"></schema-form-widget>
 */

@Component({
  selector: "schema-form-widget",
  templateUrl: "./schema-form-widget.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SchemaFormWidgetComponent extends BaseWidget implements OnInit, OnChanges {

  @ViewChild("componentHost", { static: true, read: ViewContainerRef })
  public componentHost: ViewContainerRef;

  public renderError: string;

  constructor(private componentFactoryResolver: ComponentFactoryResolver, private schemaFormWidgetsRegistry: SchemaFormWidgetsRegistry) {
    super();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.schema)
      this.evalSchema();
  }

  public ngOnInit() {
    this.evalSchema();
  }

  private evalSchema() {
    this.componentHost.clear();
    if (this.schema?.type) {
      const component: any = this.schemaFormWidgetsRegistry.getWidgetBySchema(this.schema);
      if (component) {
        const createdComponent = this.componentHost.createComponent<BaseWidget>(this.componentFactoryResolver.resolveComponentFactory(component));
        createdComponent.instance.schema = this.schema;
        createdComponent.instance.data = this.data;
        createdComponent.instance.name = this.name;
        createdComponent.instance.options = this.options;
        createdComponent.instance.dataChange.subscribe((d: any) => {
          this.dataChange.emit(d);
        });
      }
      else {
        Logger.error("SchemaFormWidget", "no widget found for type " + this.schema.type + ", x-schema-form.widget: " + this.schema["x-schema-form"]?.widget);
        this.renderError = "no widget found for type " + this.schema.type;
      }
    }
  }
}
