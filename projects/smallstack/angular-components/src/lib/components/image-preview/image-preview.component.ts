import { Component, Inject, Input, Optional } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";

export interface ImagePreviewData {
  imageUrl: string;
  title?: string;
}

@Component({
  templateUrl: "./image-preview.component.html",
  styleUrls: ["./image-preview.component.scss"]
})
export class ImagePreviewComponent {

  public imageUrl: SafeUrl;

  constructor(@Optional() @Inject(MAT_DIALOG_DATA) public data: ImagePreviewData, domSanitizer: DomSanitizer) {
    this.imageUrl = domSanitizer.bypassSecurityTrustStyle("url(" + data.imageUrl + ")");
  }
}
