import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from "@angular/core";

interface Locale {
  locale: string;
  country: string;
  language: string;
}

@Component({
  selector: "locale-selector-view",
  templateUrl: "locale-selector-view.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LocaleSelectorView {
  public splitLocales: Locale[];
  public country: string;
  public language: string;
  public locale: string;

  @Input() public menuClass: string;
  @Input() public displayMode: string = "full";

  @Input("locales")
  public set locales(locales: string[]) {
    if (locales)
      this.splitLocales = locales
        .map((locale) => ({ locale, ...splitLocale(locale) }));
  }

  @Input("locale")
  public set currentLocale(locale: string) {
    if (locale) {
      const [lang, country] = locale.split("_");
      this.locale = locale;
      this.language = lang;
      this.country = country;
    }
  }

  @Output() public change = new EventEmitter<string>();
}

function splitLocale(locale: string): { language: string, country: string } {
  const [language, country] = locale.split("_");
  return { language, country };
}
