import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation } from "@angular/core";
import { currentLocale$, LocaleService } from "../../services/locale.service";

@Component({
  selector: "locale-selector",
  templateUrl: "./locale-selector.component.html",
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LocaleSelectorComponent {

  @Input()
  public locales: string[];

  @Input()
  public menuClass: string;

  /**
   * can be 'full' or 'flagOnly'
   */
  @Input()
  public displayMode: string = "full";

  @Output()
  public selectedLocale: EventEmitter<string> = new EventEmitter();

  public currentLocale$ = currentLocale$;

  constructor(private localeService: LocaleService) {
  }

  public selectLocale(language: string) {
    this.localeService.setCurrentLocale(language);
    this.selectedLocale.emit(language);
  }
}
