import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, HostListener, Inject, Input, Optional } from "@angular/core";
import { MatSelectChange } from "@angular/material/select";
import { ActivatedRoute, Params } from "@angular/router";
import { LogicalOperator, SearchByField, SearchByFieldMatcher, SQBuilder } from "@smallstack/cloud-api-client";
import { IOC } from "@smallstack/core-common";
import { PageableStore } from "@smallstack/store";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { WebComponentService } from "../../interfaces/web-component-service";
import { IS_MOBILE } from "../../services/breakpoint.service";
import { TranslationService } from "../../services/translation.service";

export const applicableMatchers: { [type: string]: string[] } = {
  string: [
    SearchByFieldMatcher.EXACT_MATCH,
    SearchByFieldMatcher.INCLUDES,
    SearchByFieldMatcher.STARTS_WITH,
    SearchByFieldMatcher.ENDS_WITH,
    SearchByFieldMatcher.EXISTS
  ],
  number: [
    SearchByFieldMatcher.EQUALS,
    SearchByFieldMatcher.GREATER_THAN,
    SearchByFieldMatcher.GREATER_THAN_EQUALS,
    SearchByFieldMatcher.LESS_THAN,
    SearchByFieldMatcher.LESS_THAN_EQUALS,
    SearchByFieldMatcher.BEFORE_RELATIVE_TIME,
    SearchByFieldMatcher.AFTER_RELATIVE_TIME,
    SearchByFieldMatcher.EXISTS
  ],
  boolean: [
    SearchByFieldMatcher.EQUALS,
    SearchByFieldMatcher.EXISTS
  ]
};

export interface FilterName {
  label: string;
  value: string;
}

export interface FilterValues {
  [filterName: string]: {
    config?: {
      allowCustomValues: boolean;
      dataType?: FilterDataType;
    };
    values?: {
      label: string;
      value: string | number | boolean;
    }[];
  };
}

export enum FilterDataType {
  STRING = "string",
  NUMBER = "number",
  BOOLEAN = "boolean"
}

@Component({
  selector: "store-search",
  templateUrl: "./store-search.component.html",
  styleUrls: ["./store-search.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StoreSearchComponent implements AfterViewInit {

  @Input()
  public showModeSwitcher: boolean = true;

  @Input()
  public store: PageableStore;

  @Input()
  public filterNames: FilterName[];

  @Input()
  public filterValues: FilterValues;

  @Input()
  public logicalOperator: LogicalOperator = "or";

  @Input()
  public fullTextSearch: string = "";

  @Input()
  public mode: string = "simple";


  public searchByFields: SearchByField[] = [];
  public currentFilterIndex: number;
  public currentPopup: string;
  public availableMatchers = [
    SearchByFieldMatcher.EXACT_MATCH,
    SearchByFieldMatcher.INCLUDES,
    SearchByFieldMatcher.STARTS_WITH,
    SearchByFieldMatcher.ENDS_WITH
  ]; // matcher "equals" for boolean and number searches is set withing the `load` method
  public params: Params;
  public translationLabelFilterNames: string;

  constructor(private eRef: ElementRef, @Inject(IS_MOBILE) public isMobile$: Observable<boolean>, @Optional() private activatedRoute: ActivatedRoute, private cdr: ChangeDetectorRef, public translationService: TranslationService) { }

  public ngOnInit() {
    if (this.filterNames?.length > 0) this.translationLabelFilterNames = this.translationService.getTranslation("storesearch.placeholder", { replacers: { value: this.getLabelFilterNames() } });
    else return;
  }

  public async ngAfterViewInit() {
    if (IOC.isRegistered("webComponentService"))
      this.params = IOC.get<WebComponentService>("webComponentService").getNavigationParams();

    if (this.params?.searchQuery) {
      const searchQueryParam = SQBuilder.fromBase64String(this.params.searchQuery);
      this.searchByFields = searchQueryParam.fieldSearches;
      this.logicalOperator = searchQueryParam.logicalOperator;
      this.mode = "advanced";
      this.cdr.markForCheck();
      await this.load();
    }
    this.store.currentSearch$.pipe(tap((search) => {
      this.searchByFields = search;
    })).subscribe();
  }

  @HostListener("document:click", ["$event"])
  public clickout(event) {
    if (!this.eRef.nativeElement.contains(event.target))
      this.currentPopup = undefined;
  }

  public async reset() {
    this.fullTextSearch = "";
    this.searchByFields = [];
    this.currentFilterIndex = undefined;
    this.currentPopup = undefined;
    this.store.query$.next({ page: 1, size: 10 }); // reload does not reset the search - has to be done seperatly
    await this.store.reload();
  }

  public addFilter(event: MatSelectChange) {
    this.searchByFields.push({
      fieldname: event.value,
      value: ""
    });
  }

  public removeFilter(index: number) {
    this.searchByFields.splice(index, 1);
  }

  public createNewFilter() {
    const length = this.searchByFields.push({} as any);
    this.triggerEditFieldName(length - 1);
  }

  public async setFilterValue(event: MouseEvent, val: string) {
    event.stopImmediatePropagation();
    event.preventDefault();
    this.searchByFields[this.currentFilterIndex].value = val;
    await this.load();
    this.currentPopup = undefined;
  }

  public getFilterValue(filterFieldName: string, filterValue: string) {
    if (this.filterValues && this.filterValues[filterFieldName]?.values instanceof Array) {
      const labeledValue = this.filterValues[filterFieldName].values.find((val) => val.value === filterValue);
      if (labeledValue)
        return labeledValue.label;
    }
    return filterValue;
  }

  public triggerEditFieldName(filterIndex: number) {
    this.currentFilterIndex = filterIndex;
    this.currentPopup = "name";
  }

  public setCurrentFieldname(event: MouseEvent, name: string) {
    event.stopImmediatePropagation();
    event.preventDefault();
    if (this.currentFilterIndex === undefined) {
      const length = this.searchByFields.push({} as any);
      this.currentFilterIndex = length - 1;
    }
    this.searchByFields[this.currentFilterIndex].fieldname = name;
    if (this.searchByFields[this.currentFilterIndex].matcher === undefined)
      this.currentPopup = "matcher";
    else
      this.currentPopup = undefined;
    this.searchByFields[this.currentFilterIndex].value = undefined;
  }

  public getFieldName(fieldName: string) {
    if (this.filterNames) {
      const labeledValue = this.filterNames.find((fn) => fn.value === fieldName);
      if (labeledValue)
        return labeledValue.label;
    }
    return fieldName;
  }

  public triggerEditFieldMatcher(filterIndex: number) {
    this.currentFilterIndex = filterIndex;
    this.currentPopup = "matcher";
  }

  public setCurrentMatcher(event: MouseEvent, matcher: SearchByFieldMatcher.STARTS_WITH | SearchByFieldMatcher.ENDS_WITH | SearchByFieldMatcher.EXACT_MATCH | SearchByFieldMatcher.INCLUDES | SearchByFieldMatcher.EQUALS) {
    event.stopImmediatePropagation();
    event.preventDefault();
    this.searchByFields[this.currentFilterIndex].matcher = matcher;
    this.currentPopup = undefined;
  }

  public changeMode(mode: string) {
    this.searchByFields = [];
    this.store.currentSearch$.next([]);
    this.mode = mode;
    this.cdr.detectChanges();
  }

  public async load() {
    // Advanced search
    // tslint:disable-next-line: curly
    if (this.mode === "advanced") {
      if (this.searchByFields instanceof Array && this.searchByFields.length > 0) {
        this.searchByFields.forEach((searchByField) => {
          if (this.filterValues && this.shouldSearchForNumeric(searchByField?.fieldname) && this.isNumber(searchByField.value))
            searchByField.value = Number(searchByField.value);
          if (this.filterValues && this.shouldSearchForBoolean(searchByField?.fieldname) && this.isBoolean(searchByField.value))
            searchByField.value = searchByField.value.toString().toLowerCase() === "true";
        });
        await this.store.searchByFields(this.searchByFields, this.logicalOperator);
      }
      else {
        // Reset on empty search
        this.store.query$.next({ page: 1, size: 10 });
        await this.store.reload();
      }
    }
    // Simple search
    // tslint:disable-next-line: curly
    else {
      if (this.fullTextSearch.length) {
        // construct fulltext search
        this.searchByFields = [];
        // create unique string token
        const tokens = this.fullTextSearch.length > 0 ? [...new Set(this.fullTextSearch.split(" ").filter(Boolean))] : [];
        tokens.forEach((token) => {
          // create a searchField for each token with respecitve matcher depending on the datatype
          if (this.filterNames instanceof Array)
            for (const fieldname of this.filterNames)
              // config.dataType: number
              if (this.filterValues && this.shouldSearchForNumeric(fieldname.value) && this.isNumber(token))
                this.searchByFields.push({ fieldname: fieldname.value, value: Number(token), caseSensitive: false, matcher: SearchByFieldMatcher.EQUALS });
              // config.dataType: boolean
              else if (this.filterValues && this.shouldSearchForBoolean(fieldname.value) && this.isBoolean(token))
                this.searchByFields.push({ fieldname: fieldname.value, value: token.toLowerCase() === "true", caseSensitive: false, matcher: SearchByFieldMatcher.EQUALS });
              // config.dataType: string
              else
                this.searchByFields.push({ fieldname: fieldname.value, value: token, caseSensitive: false, matcher: SearchByFieldMatcher.INCLUDES });
        });
        await this.store.searchByFields(this.searchByFields, "or");
      }
      else
        // Reset on empty search
        await this.reset();
    }
  }

  public applicableMatches(searchByField: SearchByField): string[] {
    if (this.filterValues) {
      // number and boolean searches require the matcher "equals"
      const dataType = this.filterValues[searchByField.fieldname]?.config?.dataType;
      if (dataType)
        return applicableMatchers[dataType];
    }
    return this.availableMatchers;
  }

  private shouldSearchForNumeric(fieldName: string): boolean {
    return this.filterValues[fieldName]?.config?.dataType === FilterDataType.NUMBER;
  }

  private isNumber(s: any): boolean {
    return s.length && !isNaN(s as any);
  }

  private shouldSearchForBoolean(fieldName: string): boolean {
    return this.filterValues[fieldName]?.config?.dataType === FilterDataType.BOOLEAN;
  }

  private isBoolean(s: any): boolean {
    return s.length && (s.toLowerCase() === "true" || s.toLowerCase() === "false");
  }

  public getLabelFilterNames(): string {
    if (this.filterNames?.length > 1) {
      const array = this.filterNames.map(filterName => filterName.label);
      return array.slice(0, -1).join(", ") + " " + this.translationService.getTranslation("common.and") + " " + array.slice(-1);
    } else if (this.filterNames?.length === 1) {
      const array = this.filterNames.map(filterName => filterName.label);
      return array[0];
    } else return;
  }

}
