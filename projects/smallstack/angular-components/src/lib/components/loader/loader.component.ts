import { Component, Input } from "@angular/core";

@Component({
  selector: "loader",
  templateUrl: "./loader.component.html",
  styleUrls: ["./loader.component.scss"]
})
export class LoaderComponent {

  @Input()
  public loading: boolean = true;

  @Input()
  public loadingText: string = "components.loader.loadingdata";
}
