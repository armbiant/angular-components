import { ChangeDetectorRef, Directive, EventEmitter, Inject, Input, Output } from "@angular/core";
import { ButtonOpts } from "./button-options.interface";

@Directive()
export class AbstractProgressButtonComponent {

  @Input("raised")
  public set _raised(raised: boolean) {
    this.options.raised = raised;
  }

  @Input("stroked")
  public set _stroked(stroked: boolean) {
    this.options.stroked = stroked;
  }

  @Input("flat")
  public set _flat(flat: boolean) {
    this.options.flat = flat;
  }

  @Input("text")
  public set _text(text: string) {
    this.options.text = text;
  }

  @Input("spinnerText")
  public set _spinnerText(spinnerText: string) {
    this.options.spinnerText = spinnerText;
  }

  @Input()
  public disabled = false;

  @Input()
  public set loading(loading: boolean) {
    if (this.options)
      this.options.loading = loading;
  }

  @Input("color")
  public set _color(color: string) {
    this.options.buttonColor = color;
  }

  @Output()
  public actionFn: EventEmitter<() => void> = new EventEmitter();

  @Input()
  public options: ButtonOpts = { loading: false, spinnerSize: 15 };

  constructor(@Inject(ChangeDetectorRef) private cdr: ChangeDetectorRef) {
  }

  public buttonClicked(e?: Event) {
    if (this.disabled || this.options.loading) {
      if (e) {
        e.preventDefault();
        e.stopPropagation();
      }
      return;
    }

    if (this.options.spinnerText === undefined)
      this.options.spinnerText = this.options.text;
    this.options.loading = true;
    try {
      this.actionFn.emit(() => {
        this.options.loading = false;
        this.cdr.detectChanges();
      });
    } catch (e) {
      this.options.loading = false;
    }
  }
}
