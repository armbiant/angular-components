import { Component, ViewEncapsulation } from "@angular/core";
import { AbstractProgressButtonComponent } from "./abstract-progress-button.component";

@Component({
  selector: "spinner-button",
  template: `
      <button mat-button
              [color]="options.buttonColor"
              [class.active]="options.loading"
              [class.fullWidth]="options.fullWidth"
              [class.mat-raised-button]="options.raised"
              [class.mat-stroked-button]="options.stroked"
              [class.mat-flat-button]="options.flat"
              [disabled]="options.loading || disabled" (click)="buttonClicked()">

          <mat-spinner class="spinner mr-4"
                        [diameter]="options.spinnerSize"
                        [color]="options.spinnerColor"
                        *ngIf="options.loading">
          </mat-spinner>

          <ng-content></ng-content>

          <span *ngIf="!options.loading">{{ options.text }}</span>
          <span *ngIf="options && options.loading">{{ options.spinnerText }}</span>


      </button>
  `,
  styles: [`
      button /deep/ .mat-button-wrapper {
          display: flex;
          align-items: center;
          justify-content: center;
      }

      button.active {
          cursor: not-allowed
      }

      .spinner {
          margin-top: 2px;
          margin-left: 5px;
      }

      .fullWidth {
          width: 100%;
      }
  `],
  encapsulation: ViewEncapsulation.None
})
export class SpinnerButtonComponent extends AbstractProgressButtonComponent { }
