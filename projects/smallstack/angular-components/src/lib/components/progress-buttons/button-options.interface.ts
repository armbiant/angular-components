export interface ButtonOpts {
  loading: boolean;
  text?: string;
  spinnerText?: string;
  buttonColor?: string;
  spinnerColor?: string;
  barColor?: string;
  raised?: boolean;
  stroked?: boolean;
  flat?: boolean;
  spinnerSize?: number;
  mode?: string;
  value?: number;
  fullWidth?: boolean;
}
