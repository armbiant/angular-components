import { Component, Input, ViewEncapsulation } from "@angular/core";
import { AbstractProgressButtonComponent } from "./abstract-progress-button.component";
import { ButtonOpts } from "./button-options.interface";

@Component({
  selector: "progress-bar-button",
  template: `
      <button mat-button
          [color]="options.buttonColor"
          [class.active]="options.loading"
          [class.mat-raised-button]="options.raised"
          [class.mat-stroked-button]="options.stroked"
          [class.mat-flat-button]="options.flat"
          [disabled]="options.loading || disabled"
          (click)="buttonClicked()">

          <ng-content></ng-content>

          <span *ngIf="!options.loading">{{ options.text }}</span>
          <span *ngIf="options.loading">{{ options.spinnerText }}</span>

          <mat-progress-bar
            *ngIf="options?.loading"
            [color]="options.barColor"
            class="bar"
            [mode]="options.mode"
            [value]="options.value">
          </mat-progress-bar>
      </button>
  `,
  styles: [`
      .bar {
          position: absolute !important;
          bottom: 0px;
          left: 0px;
      }
  `],
  encapsulation: ViewEncapsulation.None
})
export class BarButtonComponent extends AbstractProgressButtonComponent {

  @Input()
  public options: ButtonOpts = { loading: false, text: "", spinnerText: "", spinnerSize: 10, mode: "buffer" };
}
