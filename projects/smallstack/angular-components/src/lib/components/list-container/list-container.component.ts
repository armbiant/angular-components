import { BreakpointObserver } from "@angular/cdk/layout";
import { Component, ContentChild, ElementRef, Input, OnChanges, OnInit, Optional, SimpleChanges, ViewChild } from "@angular/core";
import { PageEvent } from "@angular/material/paginator";
import { PageableCrudStore, PageableStore, StoreState } from "@smallstack/store";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { PAGINATOR_POSITION } from "../../constants";
import { BackofficeErrorMessageHandler } from "../../services/backoffice-error-handler";

/**
 * @example
 * <list-container></list-container>
 */
@Component({
  selector: "list-container",
  templateUrl: "./list-container.component.html",
  styleUrls: ["./list-container.component.scss"]
})
export class ListContainerComponent implements OnInit, OnChanges {

  @Input()
  public paginatorPosition?: PAGINATOR_POSITION = PAGINATOR_POSITION.BOTTOM;
  public PAGINATOR_POSITION = PAGINATOR_POSITION; // Make enum accessible in template

  @Input()
  public set showEmpty(val: boolean) {
    // @ts-ignore val can be a string if someone accidentaly set `showEmpty="true"` instead of `[showEmpty]="true"`
    this.showEmptyInternal = val || val === "true";
  }

  public get showEmpty(): boolean {
    return this.showEmptyInternal;
  }

  public isPageable: boolean;
  public isMobile: boolean;

  @Input()
  public store: PageableCrudStore | PageableStore;

  public totalElements$: Observable<number>;
  public size$: Observable<number>;
  public pageIndex$: Observable<number>;

  public storeError$: Observable<string>;

  @ContentChild("empty", { static: false })
  public empty: ElementRef;

  @ContentChild("content", { static: false })
  public content: ElementRef;

  @ViewChild("legacyNgContent")
  private legacyNgContent: ElementRef;

  private showEmptyInternal: boolean = true;

  constructor(breakpointObserver: BreakpointObserver, @Optional() private errorMessageHandler: BackofficeErrorMessageHandler) {
    this.isMobile = breakpointObserver.isMatched("(max-width: 599px)");
  }

  public projectContentChanged(): void {
    if (this.legacyNgContent?.nativeElement?.children.length > 0) {
      for (const child of this.legacyNgContent.nativeElement.children)
        // tslint:disable-next-line: no-console
        console.error("Found invalid ng-content element: ", child);
      throw new Error("Please use <ng-template #content>...</ng-template> inside <list-container> instead of ng-content! Using ng-content in <list-container> will be deprecated soon and therefor will result in an empty component!");
    }
  }

  public ngOnInit(): void {
    this.isPageable = this.store instanceof PageableCrudStore || this.store instanceof PageableStore;
    this.totalElements$ = this.store?.pagination$.pipe(map((v) => v.totalElements));
    this.size$ = this.store?.query$.pipe(map((v) => v.size));
    this.pageIndex$ = this.store?.query$.pipe(map((v) => v.page - 1));
    this.storeError$ = this.store?.error$.pipe(map((error) => {
      if (this.errorMessageHandler)
        return this.errorMessageHandler.handleMessage(error);
      return JSON.stringify(error);
    }));
  }

  public async ngOnChanges(changes: SimpleChanges) {
    if (changes.store.currentValue)
      if (this.store.state === StoreState.INITIAL)
        await this.store.load();
  }

  public hasUserDefinedEmptyContent(): boolean {
    return this.empty !== undefined;
  }

  public async changePage(page: PageEvent) {
    if (this.store instanceof PageableCrudStore || this.store instanceof PageableStore) {
      const currentQuery = this.store?.query$?.value;
      this.store.query$.next({ page: page.pageIndex + 1, size: page.pageSize, search: currentQuery?.search, sort: currentQuery?.sort });
      await this.store.load();
    }
  }


}
