import { MatPaginatorIntl } from "@angular/material/paginator";
import { currentLocale$ } from "../../services/locale.service";
import { TranslationService } from "../../services/translation.service";

export function PaginatorTranslation(translationService: TranslationService) {
  const paginatorTranslationIntl = new MatPaginatorIntl();

  function translations() {
    paginatorTranslationIntl.itemsPerPageLabel = translationService.getTranslation("pager.itemsperpagelabel");
    paginatorTranslationIntl.firstPageLabel = translationService.getTranslation("pager.firstpagelabel");
    paginatorTranslationIntl.nextPageLabel = translationService.getTranslation("pager.nextpagelabel");
    paginatorTranslationIntl.previousPageLabel = translationService.getTranslation("pager.previouspagelabel");
    paginatorTranslationIntl.lastPageLabel = translationService.getTranslation("pager.lastpagelabel");

    const of = translationService.getTranslation("pager.of");

    paginatorTranslationIntl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      if (length === 0 || pageSize === 0) return "0 " + of + " " + length;
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      const endIndex = startIndex < length ?
        Math.min(startIndex + pageSize, length) :
        startIndex + pageSize;
      return startIndex + 1 + " – " + endIndex + " " + of + " " + length;
    };
  }

  currentLocale$.subscribe(() => {
    translations();
    paginatorTranslationIntl.changes.next();
  });

  return paginatorTranslationIntl;
}