import { Component, EventEmitter, Input, Output } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { MatDatepickerInputEvent } from "@angular/material/datepicker";
import moment from "moment";
import { range } from "underscore";

@Component({
  selector: "timestamp-editor",
  templateUrl: "./timestamp-editor.component.html"
})

export class TimestampEditorComponent {

  @Input()
  public timestamp: number;

  @Output()
  public timestampChange = new EventEmitter<number>();

  public showTimezoneChanger = false;

  public date: moment.Moment = moment();

  public get hours(): number {
    return this.date.get("hours");
  }

  public set hours(hours: number) {
    this.date.set("hours", hours);
  }

  public get minutes(): number {
    return this.date.get("minutes");
  }

  public set minutes(minutes: number) {
    this.date.set("minutes", minutes);
  }

  public minuteForm: FormControl;
  public hourForm: FormControl;
  public selectedTimezone: number;
  public timezones: number[] = range(-12, 13);

  public constructor() {
    this.selectedTimezone = this.date.utcOffset() / 60;
    this.minuteForm = new FormControl("", Validators.compose([
      Validators.min(0),
      Validators.max(59),
      Validators.pattern("^[0-9]{1,2}$")
    ]));
    this.hourForm = new FormControl("", Validators.compose([
      Validators.min(0),
      Validators.max(23),
      Validators.pattern("^[0-9]{1,2}$")
    ]));
  }

  public ngOnChanges() {
    if (this.timestamp) {
      const incomingTimestamp = moment(this.timestamp);
      incomingTimestamp.utcOffset(this.selectedTimezone * 60);
      this.date = incomingTimestamp;
    }
  }

  public save() {
    const output = moment(this.date);
    output.set("second", 0);
    output.set("millisecond", 0);
    this.timestampChange.emit(output.valueOf());
  }

  public dateChanged(changed: MatDatepickerInputEvent<moment.Moment>) {
    this.date = changed.value;
  }

  public setSelectedTimezone(offset: number) {
    this.date.utcOffset(offset);
    this.selectedTimezone = offset;
  }

}
