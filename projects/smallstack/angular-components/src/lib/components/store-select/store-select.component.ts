import { Component, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from "@angular/core";
import { MatMenu, MatMenuTrigger } from "@angular/material/menu";
import { PageEvent } from "@angular/material/paginator";
import { PageableCrudStore, PageableStore, StoreState } from "@smallstack/store";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { FilterName } from "../store-search/store-search.component";


/**
 * @example
 * <store-select [store]="myStore" [multiple]="true" property="name" label="Select from My Store" [(selectedIds)]="selectedIds"></store-select>
 */

@Component({
  selector: "store-select",
  templateUrl: "./store-select.component.html",
  styleUrls: ["./store-select.component.scss"]
})
export class StoreSelectComponent implements OnInit {

  @ViewChild(MatMenuTrigger)
  public menuTrigger: MatMenuTrigger;


  @Input()
  public store: PageableStore | PageableCrudStore;
  @Input()
  public multiple: boolean = false;
  @Input()
  public property: string;
  @Input()
  public label = "";
  @Input()
  public propertyLength = 30;
  @Input("class")
  public classes: string = "mat-button mat-flat-button mat-primary";
  @Input()
  public selectedIds = [];
  @Input()
  public showData: boolean = false;
  @Output()
  public selectedIdsChange = new EventEmitter<string | string[]>();

  public isPageable: boolean;
  public totalElements$: Observable<number>;
  public size$: Observable<number>;
  public pageIndex$: Observable<number>;
  public storeError$: Observable<string>;

  public filterNames: FilterName[] = [];
  public currentPage: any[];
  public currentPageSelected = false;

  @HostListener("click", ["$event"])
  public onClick(e: MouseEvent) {
    e.stopPropagation();
    e.preventDefault();

    return false;
  }

  public async ngOnInit() {
    await this.resetStore();
    this.store.currentPage$.subscribe((v) => this.currentPage = v);
    this.isPageable = this.store instanceof PageableCrudStore || this.store instanceof PageableStore;
    this.totalElements$ = this.store?.pagination$.pipe(map((v) => v.totalElements));
    this.size$ = this.store?.query$.pipe(map((v) => v.size));
    this.pageIndex$ = this.store?.query$.pipe(map((v) => v.page - 1));
    this.filterNames.push({ label: this.property, value: this.property });
    this.checkCurrentPageSelectionStatus();
  }

  public async resetStore() {
    this.store.query$.next({ page: 1, size: 10 });
    await this.store.load();
    this.checkCurrentPageSelectionStatus();
  }

  public async openMenu() {
    await this.resetStore();
    this.menuTrigger.openMenu();
  }

  public onSubmitMultiple(): void {
    this.selectedIdsChange.emit(this.selectedIds);
    this.menuTrigger.closeMenu();
  }

  public onSubmitSingle(selection: string): void {
    this.selectedIdsChange.emit(selection);
    this.menuTrigger.closeMenu();
  }

  public isSelected(id: string): boolean {
    return this.selectedIds?.includes(id);
  }

  public onToggleSelection(checked: boolean, id: string): void {
    if (this.selectedIds === undefined)
      this.selectedIds = [];
    if (checked) {
      if (!this.selectedIds?.includes(id))
        this.selectedIds = [...this.selectedIds, id];
    } else
      this.selectedIds = this.selectedIds.filter((e) => e !== id);
    this.checkCurrentPageSelectionStatus();
  }

  public async onChangePage(page: PageEvent) {
    if (this.store instanceof PageableCrudStore || this.store instanceof PageableStore) {
      const currentQuery = this.store?.query$?.value;
      this.store.query$.next({ page: page.pageIndex + 1, size: page.pageSize, search: currentQuery?.search, sort: currentQuery?.sort });
      await this.store.load();
    }
    this.checkCurrentPageSelectionStatus();
  }

  public checkCurrentPageSelectionStatus() {
    // check if all items are checked on page
    if (this.currentPage) {
      let allSelected = true;
      this.currentPage.forEach((o) => {
        if (!this.isSelected(o.id))
          allSelected = false;
      });
      this.currentPageSelected = allSelected;
    }
  }

  public onSelectCurrentPage(): void {
    // tslint:disable-next-line: curly
    if (this.currentPage) {
      this.currentPage.forEach((o) => {
        if (!this.isSelected(o.id))
          this.selectedIds = [...this.selectedIds, o.id];
      });
    }
  }

  public onToggleCurrentPage(): void {
    if (this.currentPage) {
      this.currentPageSelected = !this.currentPageSelected;
      if (this.currentPageSelected)
        this.currentPage.forEach((o) => {
          if (!this.isSelected(o.id))
            this.selectedIds = [...this.selectedIds, o.id];
        });
      else
        this.currentPage.forEach((o) => {
          this.selectedIds = this.selectedIds.filter((e) => e !== o.id);
        });
    }
  }


}
