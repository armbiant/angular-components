import { ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Translation } from "@smallstack/cloud-api-client";
import { StoreState } from "@smallstack/store";
import { Subscription } from "rxjs";
import { first, tap } from "rxjs/operators";
import { currentEditorLocale$, TranslationStore } from "../../stores";
import { LocaleStore } from "../../stores/locale.store";
import { II18nEditorDialogComponent, II18nEditorDialogData, II18nEditorDialogResult } from "./ii18n-editor-dialog.component";

/**
 * Component causes ExpressionChangedAfterItHasBeenCheckedError, see https://gitlab.com/smallstack/projects/cloud-backoffice/-/issues/465
 */

@Component({
  selector: "ii18n-editor",
  templateUrl: "./ii18n-editor.component.html",
  styleUrls: ["./ii18n-editor.component.scss"]
})
export class II18nEditorComponent implements OnInit, OnDestroy {

  /**
   * can be used to (temporarily) disabled the cog icon
   */
  @Input()
  public disableConfigBtn: boolean = true;

  @Input()
  public key: string | any;

  @Input()
  public defaultKey: string;

  @Input()
  public label: string;

  @Input()
  public hint: string;

  @Input()
  public textarea: boolean = false;

  @Output()
  public keyChange: EventEmitter<string> = new EventEmitter();

  public isTranslatedField: boolean = false;
  public currentEditorLocale: string;
  public locales: string[];
  public translation: Translation = { values: {} } as any;

  private subscription: Subscription = new Subscription();

  constructor(private matDialog: MatDialog, private localeStore: LocaleStore, private translationStore: TranslationStore, private cdr: ChangeDetectorRef) {
  }

  public async ngOnInit() {
    await this.loadLocales();
    if (this.key === undefined)
      this.key = this.defaultKey;
    if (typeof this.key === "string") {
      if (this.key.startsWith("@@")) {
        this.isTranslatedField = true;
        await this.loadValues(this.key.substring(2));
      }
    }
    else if (typeof this.key === "object" && typeof this.key.key === "string") {
      // its an ii18n expanded value
    }
    this.subscription.add(currentEditorLocale$.value$.subscribe((cel) => { this.currentEditorLocale = cel; }));
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public valueChanged(e: string) {
    this.keyChange.emit(e);
  }

  public updateValues() {
    this.key = {
      key: "@@" + this.translation.key,
      values: this.translation.values
    };
    this.keyChange.emit(this.key);
  }

  public openI18nDialog() {
    let key: string;
    if (typeof this.key === "string")
      key = this.key;
    else if (typeof this.key === "object" && typeof this.key.key === "string")
      key = this.key.key;
    this.matDialog.open(II18nEditorDialogComponent, { data: { key, isTranslatedField: this.isTranslatedField, defaultKey: this.defaultKey } as II18nEditorDialogData, width: "600px", autoFocus: false }).afterClosed().subscribe((res: II18nEditorDialogResult) => {
      if (res?.key !== undefined) {
        this.keyChange.emit(res.key);
        this.isTranslatedField = res.key.startsWith("@@");
        if (this.isTranslatedField) {
          this.translation.key = res.key.substr(2);
          this.updateValues();
        } else
          this.translation.key = res.key;
      }
    });
  }

  public setCurrentEditorLocale(locale: string) {
    currentEditorLocale$.setValue(locale);
  }

  private async loadValues(key: string) {
    const translation = await this.translationStore.getByKey(key);
    if (translation)
      this.translation = translation;
    else
      this.translation = { key: this.key.substring(2), values: {} };
    this.cdr.markForCheck();
  }

  private async loadLocales() {
    if (this.localeStore.state === StoreState.INITIAL)
      await this.localeStore.load();
    this.localeStore.value$.pipe(
      first((l) => l !== undefined),
      tap((locales) => this.locales = locales?.map((l) => l.iso))
    ).subscribe();
  }

}
