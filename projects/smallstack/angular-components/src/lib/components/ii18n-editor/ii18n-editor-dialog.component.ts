import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatSlideToggleChange } from "@angular/material/slide-toggle";

export interface II18nEditorDialogData {
  key: string;
  defaultKey: string;
}

export interface II18nEditorDialogResult {
  key: string;
}


@Component({
  templateUrl: "./ii18n-editor-dialog.component.html",
  styleUrls: ["./ii18n-editor-dialog.component.scss"]
})
export class II18nEditorDialogComponent {

  public isTranslatedField: boolean = false;
  public isCustomKey: boolean = false;

  constructor(@Inject(MAT_DIALOG_DATA) public data: II18nEditorDialogData, public dialogRef: MatDialogRef<II18nEditorDialogComponent, II18nEditorDialogResult>) {
    if (data?.key?.startsWith("@@")) {
      this.isTranslatedField = true;
      this.isCustomKey = this.data.key !== this.data.defaultKey;
    }

  }

  public close() {
    this.dialogRef.close({ key: this.data?.key });
  }

  public cancel() {
    this.dialogRef.close();
  }

  public modeChange(event: MatSlideToggleChange) {
    this.isTranslatedField = event.checked;
    if (this.isTranslatedField) {
      if (!this.data.key.startsWith("@@"))
        this.data.key = this.data.defaultKey;
      this.isCustomKey = this.data.key !== this.data.defaultKey;
    }
    else
      this.data.key = "";
  }

  public lockKey() {
    this.data.key = this.data.defaultKey;
    this.isCustomKey = false;
  }

  public unlockKey() {
    this.isCustomKey = true;
  }

}
