import { WidgetLibraryService } from "@ajsf/core";
import { MaterialDesignFrameworkModule } from "@ajsf/material";
import { ObserversModule } from "@angular/cdk/observers";
import { ScrollingModule } from "@angular/cdk/scrolling";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from "@angular/material-moment-adapter";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatButtonModule } from "@angular/material/button";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from "@angular/material/core";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatDialogModule } from "@angular/material/dialog";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatMenuModule } from "@angular/material/menu";
import { MatPaginatorIntl, MatPaginatorModule } from "@angular/material/paginator";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatRadioModule } from "@angular/material/radio";
import { MatSelectModule } from "@angular/material/select";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatTableModule } from "@angular/material/table";
import { MatTooltipModule } from "@angular/material/tooltip";
import { PdfViewerModule } from "ng2-pdf-viewer";
import { FlagComponent } from "./components/flag/flag.component";
import { ForeignIdSelectComponent } from "./components/foreign-id-select/foreign-id-select.component";
import { DocumentWidgetComponent, DrawingPadWidgetComponent, FormRendererComponent, ImageUploadWidgetComponent, LinkViewDialogComponent } from "./components/form-renderer";
import { ContactPersonSelectComponent } from "./components/form-renderer/widgets/contact-person-widget/contact-person-select/contact-person-select.component";
import { ContactPersonWidgetComponent } from "./components/form-renderer/widgets/contact-person-widget/contact-person-widget.component";
import { I18nComponent } from "./components/i18n/i18n.component";
import { II18nEditorDialogComponent } from "./components/ii18n-editor/ii18n-editor-dialog.component";
import { II18nEditorComponent } from "./components/ii18n-editor/ii18n-editor.component";
import { II18nComponent } from "./components/ii18n/ii18n.component";
import { ImagePreviewComponent } from "./components/image-preview/image-preview.component";
import { JsonStringPipe } from "./components/jsonstring/json-string.component";
import { ListContainerComponent } from "./components/list-container/list-container.component";
import { PaginatorTranslation } from "./components/list-container/paginator-translation";
import { LoaderComponent } from "./components/loader/loader.component";
import { LocaleSelectorView } from "./components/locale-selector/locale-selector-view.component";
import { LocaleSelectorComponent } from "./components/locale-selector/locale-selector.component";
import { NotificationDialogComponent } from "./components/notification-dialog/notification-dialog.component";
import { BarButtonComponent } from "./components/progress-buttons/bar-button.component";
import { SpinnerButtonComponent } from "./components/progress-buttons/spinner-button.component";
import { QRCodeDialogComponent } from "./components/qr-code-dialog/qr-code-dialog.component";
import { RefreshDialogComponent } from "./components/refresh-dialog/refresh-dialog.component";
import { BaseWidget, CheckboxWidgetComponent, InputWidgetComponent, IntegerWidgetComponent, SchemaFormWidgetComponent, StringSelectWidgetComponent } from "./components/schema-form";
import { AddSchemaEntryComponent } from "./components/schema-form/add-schema-entry/add-schema-entry.component";
import { SchemaFormLayoutComponent } from "./components/schema-form/schema-form-layout/schema-form-layout.component";
import { SchemaFormPlaceholderDirective } from "./components/schema-form/schema-form-layout/schema-form-placeholder.directive";
import { SchemaFormTableComponent } from "./components/schema-form/schema-form-table/schema-form-table.component";
import { SchemaFormWidgetsRegistry } from "./components/schema-form/schema-form-widget-registry";
import { ArrayWidgetComponent } from "./components/schema-form/widgets/array-widget/array-widget.component";
import { NumberWidgetComponent } from "./components/schema-form/widgets/number-widget/number-widget.component";
import { ObjectWidgetComponent } from "./components/schema-form/widgets/object-widget/object-widget.component";
import { SearchQueryWidgetComponent } from "./components/schema-form/widgets/search-query-widget/search-query-widget.component";
import { StoreSelectWidgetComponent } from "./components/schema-form/widgets/store-select-widget/store-select-widget.component";
import { SearchFieldComponent } from "./components/search-field/search-field.component";
import { SimpleStoreTableComponent } from "./components/simple-store-table/simple-store-table.component";
import { StoreContainerComponent } from "./components/store-container/store-container.component";
import { StoreModelComponent } from "./components/store-model/store-model.component";
import { StorePropertyComponent } from "./components/store-property/store-property.component";
import { StoreSearchComponent } from "./components/store-search/store-search.component";
import { StoreSelectComponent } from "./components/store-select/store-select.component";
import { TimestampEditorComponent } from "./components/timestamp-editor/timestamp-editor.component";
import { VerificationMessageComponent } from "./components/verification-message/verification-message.component";
import { MatSortStoreDirective } from "./directives/mat-sort-store/mat-sort-store.directive";
import { SacAutofocusDirective } from "./directives/sac-autofocus.directive";
import { II18nPipe } from "./pipes/ii18n.pipe";
import { TimezonePipe } from "./pipes/timezone.pipe";
import { BreakpointService, IS_MOBILE } from "./services/breakpoint.service";
import { SwUpdateService } from "./services/sw-update.service";
import { TranslationService } from "./services/translation.service";


export function isMobileFactory(breakpointService: BreakpointService) {
  return breakpointService.isMobile$;
}

export function paginatorTranslation(translationService: TranslationService) {
  return PaginatorTranslation(translationService);
}
@NgModule({
  declarations: [
    ListContainerComponent,
    I18nComponent,
    II18nComponent,
    LoaderComponent,
    StoreContainerComponent,
    StorePropertyComponent,
    SpinnerButtonComponent,
    BarButtonComponent,
    ForeignIdSelectComponent,
    StoreSearchComponent,
    SearchFieldComponent,
    SimpleStoreTableComponent,
    LocaleSelectorComponent,
    LocaleSelectorView,
    FlagComponent,
    JsonStringPipe,
    NotificationDialogComponent,
    StoreModelComponent,
    II18nPipe,
    TimezonePipe,
    StoreSelectComponent,
    ImagePreviewComponent,
    MatSortStoreDirective,
    FormRendererComponent,
    ContactPersonSelectComponent,
    ContactPersonWidgetComponent,
    ImageUploadWidgetComponent,
    DrawingPadWidgetComponent,
    DocumentWidgetComponent,
    LinkViewDialogComponent,
    RefreshDialogComponent,
    QRCodeDialogComponent,
    II18nEditorComponent,
    II18nEditorDialogComponent,
    InputWidgetComponent,
    SchemaFormWidgetComponent,
    CheckboxWidgetComponent,
    NumberWidgetComponent,
    SchemaFormLayoutComponent,
    SchemaFormPlaceholderDirective,
    SchemaFormTableComponent,
    IntegerWidgetComponent,
    AddSchemaEntryComponent,
    StringSelectWidgetComponent,
    TimestampEditorComponent,
    StoreSelectWidgetComponent,
    SearchQueryWidgetComponent,
    ObjectWidgetComponent,
    VerificationMessageComponent,
    ArrayWidgetComponent,
    SacAutofocusDirective
  ],
  imports: [
    MatDatepickerModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatPaginatorModule,
    MatDialogModule,
    MatTooltipModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatTableModule,
    FlexLayoutModule,
    MatMenuModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    ObserversModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatCheckboxModule,
    MaterialDesignFrameworkModule,
    PdfViewerModule,
    ScrollingModule,
    MatSlideToggleModule
  ],
  exports: [
    ListContainerComponent,
    I18nComponent,
    II18nComponent,
    LoaderComponent,
    StoreContainerComponent,
    StorePropertyComponent,
    SpinnerButtonComponent,
    BarButtonComponent,
    ForeignIdSelectComponent,
    StoreSearchComponent,
    SearchFieldComponent,
    SimpleStoreTableComponent,
    LocaleSelectorComponent,
    FlagComponent,
    JsonStringPipe,
    NotificationDialogComponent,
    StoreModelComponent,
    II18nPipe,
    TimezonePipe,
    StoreSelectComponent,
    ImagePreviewComponent,
    MatSortStoreDirective,
    FormRendererComponent,
    ContactPersonSelectComponent,
    QRCodeDialogComponent,
    II18nEditorComponent,
    II18nEditorDialogComponent,
    InputWidgetComponent,
    SchemaFormWidgetComponent,
    CheckboxWidgetComponent,
    NumberWidgetComponent,
    SchemaFormLayoutComponent,
    SchemaFormPlaceholderDirective,
    SchemaFormTableComponent,
    IntegerWidgetComponent,
    StringSelectWidgetComponent,
    TimestampEditorComponent,
    SearchQueryWidgetComponent,
    ObjectWidgetComponent,
    VerificationMessageComponent,
    ArrayWidgetComponent,
    SacAutofocusDirective
  ],
  providers: [
    SchemaFormWidgetsRegistry,
    BreakpointService,
    WidgetLibraryService,
    SwUpdateService,
    {
      provide: IS_MOBILE,
      useFactory: isMobileFactory,
      deps: [BreakpointService]
    },
    { provide: MAT_DATE_LOCALE, useValue: "de" },
    { provide: DateAdapter, useClass: MomentDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    {
      provide: MatPaginatorIntl, useFactory: paginatorTranslation,
      deps: [TranslationService]
    }
  ]
})
export class AngularComponentsModule {
  constructor(public breakpointService: BreakpointService) { }
}
