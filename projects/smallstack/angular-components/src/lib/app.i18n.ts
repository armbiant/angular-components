import { TranslationService } from "./services/translation.service";

export function addComponentTranslations(translationService: TranslationService) {

  // COMMON
  translationService.addTranslation(["en_us", "de_de", "fr_fr", "es_es"], {
    demo: {
      success: ["Awesome job", "Das hat geklappt", "Succès", ""]
    }
  });

  translationService.addTranslation(["en_us", "de_de"], {
    common: {
      close: ["Close", "Schließen"],
      dismiss: ["Close", "Schließen"],
      add: ["Add", "Hinzufügen"],
      ok: ["ok", "ok"],
      refresh: ["Refresh", "Aktualisieren"],
      select: ["Select", "Auswählen"],
      and: ["and", "und"]
    },
    notifications: {
      error: {
        title: ["Error", "Fehler"]
      },
      success: {
        title: ["Success", "Erfolgreich"]
      }
    },
    countries: {
      de: ["Germany", "Deutschland"],
      ch: ["Switzerland", "Schweiz"],
      gb: ["Great Britain", "Großbritannien"],
      us: ["United States", "Vereinigte Staaten"],
      it: ["Italy", "Italien"],
      pl: ["Poland", "Polen"],
      es: ["Spain", "Spanien"],
      fr: ["France", "Frankreich"]
    },
    pager: {
      itemsperpagelabel: ["Items per Page", "Artikel pro Seite"],
      firstpagelabel: ["First Page", "Erste Seite"],
      nextpagelabel: ["Next Page", "Nächste Seite"],
      previouspagelabel: ["Previous Page", "Vorherige Seite"],
      lastpagelabel: ["Last Page", "Letzte Seite"],
      of: ["of", "von"]
    },
    storesearch: {
      placeholder: ["Search by ${value}", "Nach ${value} suchen"]
    }
  });

  // ERRORS
  translationService.addTranslation(["en_us", "de_de"], {
    errors: {
      http: {
        400: ["The request was invalid", "Die Anfrage konnte nicht verarbeitet werden"],
        401: ["You are not logged in", "Sie sind nicht eingeloggt"],
        403: ["Sorry, you don't have access to this resource", "Kein Zugriff auf diese Resource"],
        404: ["Sorry, the requested resource does not exist", "Die angefragte Resource existiert nicht"],
        409: ["The resource exists already", "Die Resource existiert bereits"],
        500: ["Something went wrong", "Da ging was schief"],
        503: ["The service is not available at this moment", "Der Dienst ist im Moment nicht verfügbar"]
      },
      validations: {
        isalpha: ["must contain only letters (a-z & A-Z)", "nur Buchstaben (a-z & A-Z) sind erlaubt"],
        isemail: ["must be a valid E-Mail", "muss gültige E-Mail sein"]
      }
    }
  });

}
