
import { Injectable } from "@angular/core";
import { CloudApiClient, Locale, Translation } from "@smallstack/cloud-api-client";
import { IOC, Logger, Page, TextReplacer, TextReplacerContext, Utils } from "@smallstack/core-common";
import { each, map } from "underscore";
import { availableLocales$, currentLocale$, LocaleService } from "./locale.service";

export interface TranslationSearchFilters {
  /**
   * e.g. ['backoffice.cms.', 'smallstack.mobile.']
   */
  startsWiths?: string[];

  /**
   * e.g. ['de', 'en'] if empty, all locales are returned
   */
  locales?: string[];
}



export interface TranslationOptions {
  /**
   * * true to show a string like \_de:components.login.btn\_
   * * false to return undefined
   * * null to show the value if key is missing
   */
  showMissingKey?: boolean;

  /**
   * if true, the all fallback locales for the current locale are being considered befor showing a missing key
   */
  showFallback?: boolean;

  locale?: string;
  replacers?: TextReplacerContext;
}


export function translate(key: string, options?: TranslationOptions): string {
  return IOC.get<TranslationService>("translationService").getTranslation(key, options);
}

@Injectable()
export class TranslationService {

  protected currentLocale: string;
  protected defaultLocale: string = "en";
  protected allLocales: Locale[];
  protected translationCache: { [key: string]: { [lang: string]: string } } = {};

  constructor(private cloudApiClient: CloudApiClient, private localeService: LocaleService) {
    currentLocale$.subscribe((lang: string) => this.currentLocale = lang);
    this.localeService.getDefaultLocale().then((defaultLocale) => this.defaultLocale = defaultLocale).catch((e) => Logger.error("TranslationService", e));
    availableLocales$.subscribe((locales) => this.allLocales = locales);
  }

  public async sync(filters?: TranslationSearchFilters): Promise<void> {
    const translations: Page<Translation> = await this.getTranslations(filters);
    // tslint:disable-next-line: curly
    for (const translation of translations.elements) {
      // tslint:disable-next-line: curly
      for (const localeKey in translation.values) {
        if (translation.values[localeKey])
          this.addToCache(localeKey, translation.key, translation.values[localeKey]);
      }
    }
  }

  public getTranslations(filters: TranslationSearchFilters): Promise<Page<Translation>> {
    return this.cloudApiClient.getTranslations();
  }

  public getTranslation(key: string, options: TranslationOptions = { showFallback: true }): string {
    if (options.locale === undefined)
      options.locale = this.currentLocale;
    if (options.locale === undefined)
      options.locale = this.defaultLocale;

    let value: string;

    // check cache
    if (this.translationCache[key] && this.translationCache[key][options.locale])
      value = this.translationCache[key][options.locale];

    // if not found
    if (!value && (options.showFallback || options.showFallback === undefined)) {
      Logger.debug("TranslationService", `Translation '${key}' not found for locale '${options.locale}', trying fallbacks!`);
      const fallbackChain = this.localeService.getFallbackChain(options.locale);
      if (fallbackChain)
        for (const fallback of fallbackChain)
          if (this.translationCache[key] && this.translationCache[key][fallback])
            value = this.translationCache[key][fallback];
    }
    // if still not found
    if (!value) {
      Logger.debug("TranslationService", `Translation '${key}' not found for locale '${options.locale}'!`);
      if (options.showMissingKey)
        return `_${options.locale}:${key}_`;
      else if (options.showMissingKey === null)
        return key;
      else
        return undefined;
    }

    // replacers
    if (options && options.replacers)
      value = TextReplacer.replace(value, options.replacers);
    return value;
  }

  public addTranslation(localeKey: string | string[], newTrans: any): void {
    map(Utils.flattenJSON(newTrans, false), (value: string | string[], key: string) => {
      // tslint:disable-next-line: curly
      if (localeKey instanceof Array) {
        if (!(value instanceof Array))
          Logger.error("TranslationService", `Importing for locales ${JSON.stringify(localeKey)} but no array as value given for key ${key}`);
        else
          each(localeKey, (lang: string, index: number) => {
            this.addToCache(lang, key, value[index]);
          });
        // tslint:disable-next-line: curly
      } else {
        if (value instanceof Array)
          Logger.error("TranslationService", `Importing for locale ${localeKey} but array as value given for key ${key}`);
        else
          this.addToCache(localeKey, key, value);
      }
    });
  }

  private addToCache(localeId: string, key: string, value: string) {
    Logger.debug("TranslationService", `Adding to cache: ${localeId} -> ${key} -> ${value} `);
    if (!this.translationCache[key])
      this.translationCache[key] = {};
    this.translationCache[key][localeId] = value;
  }


}
