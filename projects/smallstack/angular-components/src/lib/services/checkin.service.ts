import { HttpClient, HttpHeaders, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CloudApiClient } from "@smallstack/cloud-api-client";
import { Utils } from "@smallstack/core-common";
import { F_CHECKIN, T_CHECKIN_VISITOR } from "../constants";


// copied from https://gitlab.com/smallstack/projects/cloud-api/blob/22ecc351504f900a8e9671b6d905e1431ada993f/src/features/checkin/checkin.feature.ts#L27
export const CHECKIN_ACTION_NAMES = {
  EVENT: "event",
  SEND_MAIL: "sendmail",
  UPLOAD_FILE: "uploadfile"
};

export interface VisitEvent {
  readonly id?: string;
  readonly createdAt?: number;
  type: string;
  visitorId: string;
  appId: string;
  formId: string;
}

export interface Visitor {
  readonly id?: string;
  readonly createdAt?: number;
  fields: any;
  lastVisitEventId?: string;
  deleteOn?: number;
  deleteOnPreference?: number;
}

@Injectable()
export class CheckinService {

  constructor(private cloudApiClient: CloudApiClient, private http: HttpClient) { }

  public async createVisitor(visitor: Visitor, meta: { appId?: string, formId?: string, createEvent?: string }): Promise<string> {

    // extract files
    const files = {};
    // tslint:disable-next-line: curly
    for (const field in visitor.fields) {
      if (typeof visitor.fields[field] === "string" && visitor.fields[field].startsWith("data:")) {
        files[field] = visitor.fields[field];
        delete visitor.fields[field];
      }
    }

    // set delete on flag?
    if (meta?.createEvent && typeof visitor.deleteOnPreference === "number")
      visitor.deleteOn = Date.now() + visitor.deleteOnPreference;

    // create visitor
    const createdVisitor: Visitor = await this.cloudApiClient.createData(visitor, F_CHECKIN, T_CHECKIN_VISITOR) as any;

    // upload files
    // tslint:disable-next-line: curly
    for (const filePropertyName in files) {
      if (files[filePropertyName]) {
        const response = await this.cloudApiClient.dispatchDataAction("uploadfile", { name: filePropertyName }, createdVisitor.id, F_CHECKIN, T_CHECKIN_VISITOR);
        if (response?.data) {
          const blob = Utils.dataURLtoBlob(files[filePropertyName]);
          const req = new HttpRequest("PUT", response?.data, blob, {
            headers: new HttpHeaders({
              "Content-Type": blob.type,
              "Content-Disposition": `attachment; filename="${filePropertyName}"`
            }),
            reportProgress: true
          });
          await this.http.request(req).toPromise();
        }
      }
    }

    // create visitor event
    if (meta?.createEvent)
      await this.createVisitEvent(createdVisitor.id, meta.createEvent, meta);
    return createdVisitor.id;
  }

  public async createVisitEvent(visitorId: string, type: string, meta: { appId?: string, formId?: string } = {}): Promise<void> {
    await this.cloudApiClient.dispatchDataAction(CHECKIN_ACTION_NAMES.EVENT, { ...meta, eventName: type }, visitorId, F_CHECKIN, T_CHECKIN_VISITOR);
  }

  public getDisplayName(visitor: Visitor): string {
    if (visitor.fields) {
      let str = "";
      // tslint:disable-next-line: curly
      for (const fieldName in visitor.fields) {
        if (visitor.fields[fieldName])
          if (fieldName.toLowerCase().includes("name") || fieldName.toLowerCase().includes("mail"))
            str += visitor.fields[fieldName] + " ";
      }
      if (str !== "")
        return str;
    }
    return visitor.id;
  }
}
