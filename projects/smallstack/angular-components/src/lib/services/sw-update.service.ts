import { ApplicationRef, Injectable } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { SwUpdate } from "@angular/service-worker";
import { Logger } from "@smallstack/core-common";
import { concat, interval } from "rxjs";
import { first } from "rxjs/operators";
import { RefreshDialogComponent } from "../components/refresh-dialog/refresh-dialog.component";



@Injectable()
export class SwUpdateService {

  constructor(
    private matDialog: MatDialog,
    private appRef: ApplicationRef,
    private swUpdate: SwUpdate
  ) {
    this.startInterval();
  }

  public checkForUpdates(): void {
    if (this.swUpdate.isEnabled)
      this.swUpdate.available.subscribe(() => this.openDialog());
  }

  private startInterval(): void {
    if (this.swUpdate.isEnabled) {
      // tslint:disable-next-line: no-boolean-literal-compare
      const appIsStable$ = this.appRef.isStable.pipe(first((isStable) => isStable === true));
      const everyHour$ = interval(60 * 60 * 1000);
      const everyHourOnceAppIsStable$ = concat(appIsStable$, everyHour$);
      everyHourOnceAppIsStable$.subscribe(() => this.swUpdate.checkForUpdate());
    }
  }

  private openDialog(): void {
    const dialogRef = this.matDialog.open(RefreshDialogComponent, { width: "350px" });
    dialogRef.afterClosed().subscribe((refresh) => {
      if (refresh)
        this.swUpdate.activateUpdate().then(() => document.location.reload()).catch((e) => Logger.error("SwUpdateService", e));
    });
  }

}
