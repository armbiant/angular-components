import { BreakpointObserver } from "@angular/cdk/layout";
import { Injectable, InjectionToken } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

export const IS_MOBILE: InjectionToken<Observable<boolean>> = new InjectionToken<Observable<boolean>>("IsMobile");


@Injectable()
export class BreakpointService {

  public isMobile$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(undefined);

  constructor(breakpointObserver: BreakpointObserver) {
    breakpointObserver.observe("(max-width: 839px)").subscribe((result) => {
      return this.isMobile$.next(result.matches);
    });
  }

}
