import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Notifier, NotifierButton } from "./notification.service";
import { translate } from "./translation.service";

@Injectable()
export class MaterialSnackbarNotifier implements Notifier {


  constructor(private snackBar: MatSnackBar) { }

  public info(title: string, message?: string, onClose?: () => void): void {
    this.openDialog(title, message, onClose, "info");
  }

  public success(title: string, message?: string, onClose?: () => void): void {
    this.openDialog(title, message, onClose, "info");
  }

  public debug(title: string, message?: string, onClose?: () => void): void {
    this.openDialog(title, message, onClose, "info");
  }

  public warn(title: string, message?: string, onClose?: () => void): void {
    this.openDialog(title, message, onClose, "info");
  }

  public error(title: string, message?: string, onClose?: () => void): void {
    this.openDialog(title, message, onClose, "error");
  }

  public confirmation(title: string, message: string, buttons: NotifierButton[], callback: (answer: any) => void): void {
    // this.openDialog(title, message, onClose, "info");
  }

  private openDialog(title: string, message: string, onClose: () => void, cssClass: string) {
    let text: string = title;
    if (message)
      text += ` - ${message}`;
    if (this.snackBar)
      this.snackBar.open(text, translate("common.ok", { showMissingKey: true }).toUpperCase(), {
        announcementMessage: text,
        duration: 10000,
        panelClass: `snackbar-${cssClass}`,
        horizontalPosition: "right",
        verticalPosition: "bottom"
      });
  }

}
