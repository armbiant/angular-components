import { Injectable } from "@angular/core";
import { CloudApiClient, Locale } from "@smallstack/cloud-api-client";
import { Logger, Page } from "@smallstack/core-common";
import { BehaviorSubject } from "rxjs";

export const currentLocale$: BehaviorSubject<string> = new BehaviorSubject<string>(undefined);
export const availableLocales$: BehaviorSubject<Locale[]> = new BehaviorSubject<Locale[]>(undefined);

const fallbackLocale = "de_de";

@Injectable()
export class LocaleService {

  private fallbackChains: { [key: string]: string[] } = {
    "de_de": ["de_de", "en_us"]
  };

  constructor(private cloudApiClient: CloudApiClient) {
  }

  public getCurrentLocale(): string {
    if (currentLocale$.value !== undefined)
      return currentLocale$.value;
    return fallbackLocale;
  }

  public setCurrentLocale(locale: string): void {
    localStorage.setItem("locale", locale);
    if (currentLocale$ === undefined)
      throw new Error("currentLocale$ is still undefined");
    currentLocale$.next(locale);
  }

  public async getDefaultLocale(): Promise<string> {
    if (!availableLocales$.value)
      await this.getAllLocales();
    const allLocales: Locale[] = availableLocales$.value;
    return allLocales[0]?.iso || fallbackLocale;
  }

  public async getAllLocales(): Promise<Page<Locale>> {
    const response = await this.cloudApiClient.getLocales();
    availableLocales$.next(response.elements);
    for (const locale of response.elements)
      this.fallbackChains[locale.iso] = await this.cloudApiClient.getLocaleFallbackChain(locale.iso);
    return response;
  }

  public getFallbackChain(locale: string): string[] {
    return this.fallbackChains[locale];
  }

  public addFallbackChain(locale: string, chain: string[]) {
    if (!chain || !locale)
      return;
    if (chain[0] !== locale)
      throw new Error("Locale chains should always start with the locale itself");
    this.fallbackChains[locale] = chain;
  }

  public detectLocale() {
    const allLocales: Locale[] = availableLocales$.value;

    if (!allLocales)
      throw new Error("please call localeService.getAllLocales() first!");

    // check local storage
    const lsLang: string = localStorage.getItem("locale");
    // tslint:disable-next-line: curly
    if (lsLang && typeof lsLang === "string") {
      if (allLocales.find((l) => l.iso === lsLang) !== undefined) {
        this.setCurrentLocale(lsLang);
        return;
      }
      else
        this.setCurrentLocale(undefined); // delete current locale if it could not be found
    }

    // browser locale
    if ("language" in navigator) {
      let language: string = navigator.language;

      if (!language)
        return;

      language = language.replace("-", "_")
        .toLocaleLowerCase();

      let locale: string;
      if (isBrowserLocale(language))
        locale = language;
      else if (isBrowserLanguage(language))
        locale = this.getLocaleForLanguage(language);

      if (!!allLocales.find((l) => l.iso === locale)) {
        this.setCurrentLocale(locale);
        return;
      }
    }

    // use default locale, but don't persist in local storage
    this.getDefaultLocale().then((d) => {
      currentLocale$.next(d);
    }).catch((e) => {
      Logger.error("LocaleService", e);
    });
  }

  public getLocaleForLanguage(language: string) {
    // TODO: how...
    switch (language) {
      case "en":
        return "en_us";
      default:
        return language + "_" + language;
    }
  }
}

function isBrowserLocale(value: string): boolean {
  return /[a-z]{2}_[a-z]{2}/.test(value);
}

function isBrowserLanguage(value: string): boolean {
  return /[a-z]{2}/.test(value);
}
