import { Injectable, NgZone } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { NotificationDialogComponent } from "../components/notification-dialog/notification-dialog.component";
import { Notifier, NotifierButton } from "./notification.service";



@Injectable()
export class MaterialDialogNotifier implements Notifier {


  constructor(private matDialog: MatDialog, private ngZone: NgZone) { }

  public info(title: string, message?: string, onClose?: () => void): void {
    this.openDialog(title, message, onClose, "info");
  }

  public success(title: string, message?: string, onClose?: () => void): void {
    this.openDialog(title, message, onClose, "info");
  }

  public debug(title: string, message?: string, onClose?: () => void): void {
    this.openDialog(title, message, onClose, "info");
  }

  public warn(title: string, message?: string, onClose?: () => void): void {
    this.openDialog(title, message, onClose, "info");
  }

  public error(title: string, message?: string, onClose?: () => void): void {
    this.openDialog(title, message, onClose, "error");
  }

  public confirmation(title: string, message: string, buttons: NotifierButton[], callback: (answer: any) => void): void {
    this.openDialog(title, message, callback, "info", {
      buttons
    });
  }

  private openDialog(title: string, message: string, onClose: (result?: any) => void, cssClass: string, additionalDialogData: any = {}) {
    this.ngZone.run(() => {
      additionalDialogData.title = title;
      additionalDialogData.message = message;
      this.matDialog.open(NotificationDialogComponent, {
        panelClass: cssClass,
        data: additionalDialogData,
        disableClose: true
      }).beforeClosed().subscribe(onClose);
    });
  }

}
