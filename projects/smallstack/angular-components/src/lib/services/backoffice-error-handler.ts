import { Injectable } from "@angular/core";
import { translate } from "./translation.service";

@Injectable()
export class BackofficeErrorMessageHandler {



  public handleMessage(message: any): string {

    if (message.rejection)
      return this.handleMessage(message.rejection);

    if (message instanceof Error) {
      if (typeof message.message === "object")
        return this.handleObject(message.message);
      // tslint:disable-next-line: curly
      if (typeof message.message === "string") {
        try {
          message.message = JSON.parse(message.message);
          return this.handleObject(message.message);
        } catch (e) {
          // can happen
        }
      }

      return `${message.name} ${message.message}`;
    }

    // try json first
    try {
      return this.handleObject(JSON.parse(message));
    } catch (e) {
      // continue
    }

    // an array with any kind of errors
    if (message instanceof Array)
      return this.handleArray(message);

    // an object with any kind of error
    if (typeof message === "object")
      return this.handleObject(message);
  }

  private handleObject(obj: any): string {

    if (typeof obj === "string" || typeof obj === "number")
      return "" + obj;

    if (obj.i18nKey)
      return translate("errors." + obj.i18nKey, { showMissingKey: true });

    if (obj.supportCode)
      return translate(`errors.${obj.code} `, { showMissingKey: true });

    // tslint:disable-next-line: curly
    if (obj.statusCode) {
      if (obj.message) {
        let msg: string | string[] = obj.message;
        if (msg instanceof Array)
          msg = this.handleArray(msg);
        if (msg.startsWith("@@"))
          msg = translate(msg.substr(2), { showMissingKey: true });
        return msg;
      }
      else
        return translate(`errors.http.${obj.statusCode}`, { showMissingKey: true });
    }

    if (obj.statusText)
      return obj.statusText;

    if (obj.responseText)
      return obj.responseText;

    if (typeof obj.message === "string")
      return obj.message;

    if (typeof obj.error === "object")
      return this.handleObject(obj.error);

    if (obj.error)
      return obj.error;

    if (obj.errors instanceof Array)
      return this.handleArray(obj.errors);

    if (obj.error)
      return obj.error;

    if (typeof obj.reason === "object")
      return this.handleObject(obj.reason);

    if (obj.reason)
      return obj.reason;

    if (typeof obj.details === "object")
      return this.handleObject(obj.details);

    if (obj.details)
      return obj.details;

    if (typeof obj.detail === "object")
      return this.handleObject(obj.detail);

    if (obj.detail)
      return obj.detail;

    // JSON Standard Error?
    if (obj.code) {
      let text: string;
      if (typeof obj.code === "string")
        text = translate("errors." + obj.code, { showMissingKey: true });
      else
        text = JSON.stringify(obj.code);
      if (obj.source && obj.source.attributes instanceof Array)
        text += ": " + obj.source.attributes.join(", ");
      return text;
    }

    // Validation Error?
    if (obj.target && obj.property && obj.constraints) {
      const constraints: string[] = [];
      for (const constraint in obj.constraints)
        if (obj.constraints[constraint])
          constraints.push(translate("errors.validations." + constraint.toLowerCase(), { showMissingKey: true }));
      return "Ungültige Eingabe \"" + obj.value + "\" für " + obj.property + ": " + constraints.join(", ");
    }

    return "Unbekannter Fehler: " + JSON.stringify(obj);
  }

  private handleArray(arr: any[]): string {
    return arr.map((val: any) => this.handleMessage(val)).join(", ");
  }

}
