import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

export const tokenChange: BehaviorSubject<boolean> = new BehaviorSubject(undefined);

@Injectable({
  providedIn: "root"
})
export class TokenService {

  private tokenIsValid: boolean;
  private localStoragePropertyName: string = "login_token";

  constructor() {
    this.setTokenValidity(this.getLoginToken() !== undefined);

    // add listener for localstorage changes
    window.addEventListener("storage", (event) => {
      if (event.key === this.localStoragePropertyName)
        if (event.newValue === null)
          this.invalidateTokenLocally();
        else
          this.setLoginToken(event.newValue);
    });
  }

  public setLoginToken(token: string): void {
    localStorage.setItem(this.localStoragePropertyName, token);
    this.setTokenValidity(true);
  }

  public getLoginToken(): string | undefined {
    const localStorageProperty = localStorage.getItem(this.localStoragePropertyName);
    if (localStorageProperty === null)
      return undefined;
    return localStorageProperty;
  }

  public invalidateTokenLocally() {
    localStorage.removeItem(this.localStoragePropertyName);
    this.setTokenValidity(false);
  }

  public setTokenValidity(valid: boolean) {
    if (valid !== this.tokenIsValid) {
      this.tokenIsValid = valid;
      tokenChange.next(this.tokenIsValid);
    }
  }

  public isTokenValid(): boolean {
    return this.tokenIsValid;
  }
}
