import { ErrorHandler, Inject, Injectable } from "@angular/core";
import * as Sentry from "@sentry/browser";
import { Logger } from "@smallstack/core-common";
import { BackofficeEnvironment, BACKOFFICE_ENVIRONMENT } from "../constants";
import { BackofficeErrorMessageHandler } from "./backoffice-error-handler";
import { MaterialDialogNotifier } from "./material-dialog.notifier";
import { MaterialSnackbarNotifier } from "./material-snackbar.notifier";
import { translate } from "./translation.service";


@Injectable()
export class BackofficeErrorHandler implements ErrorHandler {

  constructor(private notificationService: NotificationService, @Inject(BACKOFFICE_ENVIRONMENT) private environment: BackofficeEnvironment) { }


  public handleError(error) {
    Sentry.captureException(error.originalError || error);
    this.notificationService.showStandardErrorPopup(error);
    if (!this.environment.production)
      throw error;
  }
}


export interface NotifierButton {
  label: string;
  value?: any;
  /**
   * @deprecated use color for customizing the button color
   */
  classes?: string;
  color?: "primary" | "accent" | "warn";
}


export interface Notifier {
  info(title: string, message?: string, onClose?: () => void): void;
  success(title: string, message?: string, onClose?: () => void): void;
  debug(title: string, message?: string, onClose?: () => void): void;
  warn(title: string, message?: string, onClose?: () => void): void;
  error(title: string, message?: string, onClose?: () => void): void;
  confirmation(title: string, message: string, buttons: NotifierButton[], callback: (answer: any) => void): void;
}


// tslint:disable:no-console
// tslint:disable:prefer-template

export class ConsoleNotifier implements Notifier {


  public info(title: string, message?: string): void {
    this.log(title, message, "info");
  }

  public success(title: string, message?: string): void {
    this.log(title, message, "success");
  }

  public debug(title: string, message?: string): void {
    this.log(title, message, "debug");
  }

  public warn(title: string, message?: string): void {
    this.log(title, message, "warn");
  }

  public error(title: string, message?: string): void {
    this.log(title, message, "error");
  }

  public confirmation(title: string, message: string, callback) {
    throw new Error("Console Notifier cannot ask questions!");
  }

  private log(title: string, message: string, level: string) {
    switch (level) {
      case "debug":
        console.debug(title + " - " + message);
        break;
      case "error":
        console.error(title + " - " + message);
        break;
      case "warn":
        console.warn(title + " - " + message);
        break;
      case "info":
      case "success":
      default:
        console.log(title + " - " + message);
    }
  }
}


@Injectable()
export class NotificationService {

  public console: Notifier;
  public popup: Notifier;
  public notification: Notifier;

  constructor(private errorMessageHandler: BackofficeErrorMessageHandler, materialSnackbarNotifier: MaterialSnackbarNotifier, materialDialogNotifier: MaterialDialogNotifier) {
    this.console = new ConsoleNotifier();
    this.popup = materialDialogNotifier;
    this.notification = materialSnackbarNotifier;
  }

  public getStandardCallback(errorMsg?: string, successMsg?: string): any {
    if (errorMsg === undefined)
      errorMsg = "Error";
    if (successMsg === undefined)
      successMsg = "Successful";

    return (error: Error, result: any) => {
      if (error !== undefined)
        this.showStandardErrorPopup(error, errorMsg);
      else
        this.notification.success(successMsg + (result !== undefined ? ` [${result}]` : ""));
    };
  }

  public showStandardErrorPopup(error: Error, additionalText?: string);
  public showStandardErrorPopup(error: Error, additionalText?: string);

  public async showStandardErrorPopup(error: any, title?: string) {
    Logger.error("NotificationService", "An Error occured: ", { title, error });
    if (!title)
      title = translate("notifications.error.title");

    const handledErrorMessage: string = this.errorMessageHandler.handleMessage(error);
    let errorMessageString: string = translate(handledErrorMessage);
    if (errorMessageString === undefined)
      errorMessageString = handledErrorMessage;
    this.popup.error(title, errorMessageString);
  }

  public setConsoleNotifier(notifier: Notifier) {
    this.console = notifier;
  }

  public setPopupNotifier(notifier: Notifier) {
    this.popup = notifier;
  }

  public setNotificationNotifier(notifier: Notifier) {
    this.notification = notifier;
  }

  public handlePromise<T>(promise: Promise<T>, options: {
    errorMsg?: string;
    successMsg?: string;
    i18nErrorKey?: string;
    i18nSuccessKey?: string;
  } = {}): Promise<T> {
    return new Promise<T>(async (resolve, reject) => {
      try {
        const returnValue: T = await promise;
        let message: string;
        if (options && options.i18nSuccessKey)
          message = translate(options.i18nSuccessKey, { showMissingKey: true });
        else if (options.successMsg)
          message = options.successMsg;
        else
          message = translate("notifications.success.title", { showMissingKey: true });
        this.notification.success(message);
        resolve(returnValue);
      } catch (e) {
        let message: string;
        if (options && options.i18nErrorKey)
          message = translate(options.i18nErrorKey, { showMissingKey: true });
        else if (options.errorMsg)
          message = options.errorMsg;
        else
          message = translate("notifications.error.title", { showMissingKey: true });
        this.showStandardErrorPopup(e, message);
        reject(message);
      }
    });
  }
}
