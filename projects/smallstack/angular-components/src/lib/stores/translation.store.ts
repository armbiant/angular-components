import { Injectable } from "@angular/core";
import { ApiQueryRequest, CloudApiClient, SQBuilder, Translation } from "@smallstack/cloud-api-client";
import { Page } from "@smallstack/core-common";
import { PageableCrudStore } from "@smallstack/store";
import { currentProjectId$ } from "./current-project.store";

@Injectable({ providedIn: "root" })
export class TranslationStore extends PageableCrudStore<Translation> {

  constructor(private cloudApiClient: CloudApiClient) {
    super();
    currentProjectId$.value$.subscribe(async () => {
      await this.reset();
    });
  }

  public async getByKey(key: string): Promise<Translation> {
    const valueFromStore: Translation = this.value?.find((t) => t.key === key);
    if (valueFromStore)
      return valueFromStore;
    return this.loadModels({ search: SQBuilder.asString([{ fieldname: "key", value: key }]) }).then((translationPage) => translationPage.elements[0]);
  }

  protected loadModels(query: ApiQueryRequest): Promise<Page<Translation>> {
    return this.cloudApiClient.getTranslations(query);
  }
  protected loadModelById(id: string): Promise<Translation> {
    return this.cloudApiClient.getTranslation(id);
  }
  protected deleteModelById(id: string): Promise<any> {
    return this.cloudApiClient.deleteTranslation(id);
  }
  protected createModel(model: Translation): Promise<Translation> {
    return this.cloudApiClient.createTranslation(model);
  }
  protected patchModel(id: string, model: Partial<Translation>): Promise<Translation> {
    return this.cloudApiClient.patchTranslation(id, model);
  }
  protected putModel(model: Translation): Promise<Translation> {
    return this.cloudApiClient.putTranslation(model.id, model);
  }


}
