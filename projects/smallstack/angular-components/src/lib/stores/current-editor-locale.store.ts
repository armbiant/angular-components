import { LocalStorageStore } from "../utils/local-storage-store";

export const currentEditorLocale$ = new LocalStorageStore<string>({
  key: "smallstack/backoffice/current-editor-locale",
  parser: "string",
  default: "de_de"
});
