import { Injectable } from "@angular/core";
import { Store } from "@smallstack/store";


export enum StoreNames {
  LOCALES = "locales"
}


@Injectable({ providedIn: "root" })
export class StoreRegistry {

  private registeredStores: { [name: string]: Store } = {};

  public registerStore(name: string, store: Store) {
    this.registeredStores[name] = store;
  }


  public getStore<STORE_TYPE = Store>(name: string): STORE_TYPE {
    return this.registeredStores[name] as any;
  }

}
