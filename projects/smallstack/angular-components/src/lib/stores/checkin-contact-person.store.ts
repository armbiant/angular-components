
import { Injectable } from "@angular/core";
import { CloudApiClient } from "@smallstack/cloud-api-client";
import { Page } from "@smallstack/core-common";
import { PageableStore } from "@smallstack/store";
import { F_CHECKIN, T_CHECKIN_CONTACTPERSON } from "../constants";

export interface CheckinContactPerson {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
}

@Injectable()
export class CheckinContactPersonStore extends PageableStore<CheckinContactPerson> {

  constructor(private apiClient: CloudApiClient) {
    super();
  }
  protected loadModels(query: any): Promise<Page<CheckinContactPerson>> {
    return this.apiClient.getTypedData<CheckinContactPerson>(F_CHECKIN, T_CHECKIN_CONTACTPERSON, query);
  }

}
