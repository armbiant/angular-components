import { Injectable } from "@angular/core";
import { ApiQueryRequest, CloudApiClient, Locale } from "@smallstack/cloud-api-client";
import { Page } from "@smallstack/core-common";
import { PageableCrudStore } from "@smallstack/store";
import { currentProjectId$ } from "./current-project.store";
import { StoreNames, StoreRegistry } from "./store.registry";


@Injectable({ providedIn: "root" })
export class LocaleStore extends PageableCrudStore<Locale> {

  constructor(private cloudApiClient: CloudApiClient, storeRegistry: StoreRegistry) {
    super();
    currentProjectId$.value$.subscribe(async () => {
      await this.reset();
    });
    storeRegistry.registerStore(StoreNames.LOCALES, this);
  }

  protected loadModels(query: ApiQueryRequest): Promise<Page<Locale>> {
    return this.cloudApiClient.getLocales(query);
  }
  protected loadModelById(id: string): Promise<Locale> {
    return this.cloudApiClient.getLocale(id);
  }
  protected deleteModelById(id: string): Promise<any> {
    return this.cloudApiClient.deleteLocale(id);
  }
  protected createModel(model: Locale): Promise<Locale> {
    return this.cloudApiClient.createLocale(model);
  }
  protected patchModel(id: string, model: Partial<Locale>): Promise<Locale> {
    return this.cloudApiClient.patchLocale(id, model);
  }
  protected putModel(model: Locale): Promise<Locale> {
    return this.cloudApiClient.putLocale(model.id, model);
  }




}
