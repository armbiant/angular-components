import { registerLocaleData } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import localeDE from "@angular/common/locales/de";
import localeEN from "@angular/common/locales/en";
import { LOCALE_ID, NgModule } from "@angular/core";
import { FlexLayoutModule, FlexModule } from "@angular/flex-layout";
import { FormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldDefaultOptions, MatFormFieldModule, MAT_FORM_FIELD_DEFAULT_OPTIONS } from "@angular/material/form-field";
import { MatIconRegistry } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatMenuModule } from "@angular/material/menu";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { BrowserModule, DomSanitizer } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { addComponentTranslations, AngularComponentsModule, LocaleService, TranslationService } from "@smallstack/angular-components";
import { CloudApiClient } from "@smallstack/cloud-api-client";
import { AppComponent } from "./app.component";

registerLocaleData(localeDE);
registerLocaleData(localeEN);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FlexLayoutModule,
    FlexModule,
    FormsModule,
    AngularComponentsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatMenuModule,
    MatInputModule,
    HttpClientModule,
    MatButtonModule
  ],
  providers: [
    LocaleService,
    TranslationService,
    {
      provide: CloudApiClient,
      useFactory: () => {
        // Sandbox, "Demo" Firma, "Test Projekt"
        return new CloudApiClient({
          baseUrl: "https://sandbox-api.smallstack.com",
          logLevel: "debug",
          resellerIdProvider: "5dde5ae4ad9b92000e8dd7b4",
          tenantIdProvider: "5e71f8f898e42e000eb8c51d"
        });
      }
    },
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: { appearance: "outline" } as MatFormFieldDefaultOptions
    },
    { provide: LOCALE_ID, useValue: "de_de" }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer, translationService: TranslationService) {
    matIconRegistry.addSvgIconSet(domSanitizer.bypassSecurityTrustResourceUrl("/assets/backoffice.min.svg"));
    addComponentTranslations(translationService);
  }
}
