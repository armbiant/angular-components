import { Component } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { currentLocale$, FilterName, LocaleStore, QRCodeDialogComponent, SchemaFormSchema, StoreNames } from "@smallstack/angular-components";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  public myTimestamp = Date.now();
  // things you might need for your components go here
  public i18nKey: string = "demo.success";

  public localeSelectorSelectedLocale: string = "de_de";
  // schema form
  public schemaFormSchema: SchemaFormSchema = {
    type: "object",
    properties: {
      firstName: {
        type: "string",
        title: "Vorname"
      },
      lastName: {
        type: "string",
        title: "Nachname"
      },
      age: {
        type: "integer"
      },
      smallstackEmployee: {
        type: "boolean",
        title: "Arbeitet bei smallstack"
      }
    }
  };
  public schemaFormSchemaData: any = {};

  // schema form widgets
  public testSchema: SchemaFormSchema = {
    type: "object",
    properties: {
      firstName: { type: "string" },
      locales: {
        type: "array",
        title: "Locale Select",
        items: { type: "string" },
        "x-schema-form": {
          widget: "storeselect",
          store: StoreNames.LOCALES,
          propertyName: "iso",
          multiple: true
        }
      } as SchemaFormSchema,
      searchQuery: {
        type: "string",
        title: "Search Query",
        "x-schema-form": {
          widget: "searchquery"
        }
      } as SchemaFormSchema,
      colors: {
        type: "array",
        items: { type: "string" }
      },
      primes: {
        type: "array",
        items: { type: "number" }
      },
      bools: {
        type: "array",
        items: { type: "boolean" }
      }
    }
  };
  public testSchemaData: any = {
    "firstName": "Mia",
    "searchQuery": {
      "fieldSearches": [
        {
          "fieldname": "profile.age",
          "type": "number",
          "value": 25,
          "caseSensitive": true,
          "matcher": "lessThanEquals"
        }
      ],
      "logicalOperator": "or"
    }
  };

  // store search
  public storeSearch$: Observable<string>;
  public filterNames: FilterName[] = [
    { label: "Nachname", value: "lastName" },
    { label: "Vorname", value: "firstName" },
    { label: "E-Mail", value: "emails.address" }
  ];

  // loader
  public loadingText: string = "Hello World!";
  public exampleEmailToken: string;

  // qr-code-dialog
  public inputQRCode: string;
  public input: string;

  // i18n
  public i18nInput: string;
  public inputI18n: string;


  constructor(public localeStore: LocaleStore, public matDialog: MatDialog) {
    currentLocale$.next("de_de");
    localeStore.setValue([{ iso: "de_de", active: true, index: 1 }, { iso: "es_es", active: false, index: 4 }, { iso: "en_gb", active: true, index: 100 }]);
    this.storeSearch$ = localeStore.query$.pipe(
      map((query) => query.search),
      map((search) => {
        if (search === undefined)
          return undefined;
        return atob(search);
      })
    );
  }

  // qr-code-dialog
  public showQRCode(value: string) {
    this.inputQRCode = value;
    if (this.inputQRCode === undefined) return;
    this.matDialog.open(QRCodeDialogComponent, { data: this.inputQRCode });
    this.input = "";
  }

  // i18n
  public getInputForI18n(value: string) {
    this.i18nInput = value;
  }

  // progress buttons
  public actionFn(doneFn: () => void) {
    setTimeout(doneFn, 1000);
  }


}
